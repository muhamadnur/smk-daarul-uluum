package com.example.smkdaarululuum;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

import java.text.SimpleDateFormat;
import java.util.Date;

public class HeaderFooterPageEvent extends PdfPageEventHelper {
    Date dateTime;

    @Override
    public void onStartPage(PdfWriter writer, Document document) {
        Rectangle rectangle = writer.getBoxSize("art");
        Font fontNormal = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL, BaseColor.BLACK);
        Font fontBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD, BaseColor.BLACK);
        Font fontBigBold = new Font(Font.FontFamily.TIMES_ROMAN, 20, Font.BOLD, BaseColor.BLACK);
        ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER,
                new Phrase("KELOMPOK BISNIS DAN MANAJEMEN,", fontBold), rectangle.getRight() / 2, rectangle.getTop(), 0);
        ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER,
                new Phrase("TEKNOLOGI INFORMASI DAN KOMUNIKASI", fontBold), rectangle.getRight() / 2, rectangle.getTop(10), 0);
        ColumnText.showTextAligned(writer.getDirectContent(),
                Element.ALIGN_CENTER, new Phrase("SMK DAARUL ULUUM", fontBigBold),
                rectangle.getRight() / 2, rectangle.getTop(25), 0);
        ColumnText.showTextAligned(writer.getDirectContent(),
                Element.ALIGN_CENTER, new Phrase("TERAKREDITASI 'A'", fontBold),
                rectangle.getRight() / 2, rectangle.getTop(35), 0);
        ColumnText.showTextAligned(writer.getDirectContent(),
                Element.ALIGN_CENTER, new Phrase("Jl. Karet Padurenan Raya No. 53 Karet Kuningan", fontNormal),
                rectangle.getRight() / 2, rectangle.getTop(45), 0);
        ColumnText.showTextAligned(writer.getDirectContent(),
                Element.ALIGN_CENTER, new Phrase("Setia Budi Jakarta Selatan 12940 - Telp. (021) 5795 0676", fontNormal),
                rectangle.getRight() / 2, rectangle.getTop(55), 0);
        ColumnText.showTextAligned(writer.getDirectContent(),
                Element.ALIGN_CENTER, new Phrase("Website: http://www.smkdu-jaksel.sch.id Email: smkdaarul.uluum1988@gmail.com", fontNormal),
                rectangle.getRight() / 2, rectangle.getTop(65), 0);

    }

    @Override
    public void onEndPage(PdfWriter writer, Document document) {
        Rectangle rectangle = writer.getBoxSize("art");
        dateTime = new Date();
        Font fontNormal = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.BLACK);
        Font fontNormal1 = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL, BaseColor.BLACK);
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE dd MMMM YYYY");
        ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT,
                new Phrase("Jakarta, " + dateFormat.format(dateTime), fontNormal), rectangle.getRight(200), rectangle.getBottom(70), 0);
        ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT,
                new Phrase("Kepala SMK DAARUL ULUUM", fontNormal1), rectangle.getRight(200), rectangle.getBottom(60), 0);
        ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_LEFT,
                new Phrase("Poppy Sopiyati, S.E.", fontNormal), rectangle.getRight(200), rectangle.getBottom(10), 0);
    }
}
