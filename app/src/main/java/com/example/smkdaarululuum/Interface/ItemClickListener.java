package com.example.smkdaarululuum.Interface;

import android.view.View;

import com.google.firebase.database.DataSnapshot;

public interface ItemClickListener {
    void onClick(View v, int adapterPosition, boolean b);

//    void onSubEkskulSelected(DataSnapshot dataSnapshot, int position);
}
