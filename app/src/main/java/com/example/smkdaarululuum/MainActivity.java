package com.example.smkdaarululuum;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.smkdaarululuum.activity.auth.LoginActivity;
import com.example.smkdaarululuum.model.Absensi;
import com.example.smkdaarululuum.model.Ekstrakurikuler;
import com.example.smkdaarululuum.model.Nilai;
import com.example.smkdaarululuum.model.Pembina;
import com.example.smkdaarululuum.model.Siswa;
import com.example.smkdaarululuum.model.SubEkstrakurikuler;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import dmax.dialog.SpotsDialog;

public class MainActivity extends AppCompatActivity {
    AlertDialog spotDialog;
    FirebaseAuth mAuth;
    private FirebaseFirestore mFirestore;
    Dialog popupAdd;
    static int PreqCode = 1;
    static int REQUESTCODE = 1;
    ImageView image_post, image_camera, cancel, image_cancel;
    private Uri pickImage = null;
    Button btn_save, btn_simpan;
    EditText et_title, et_deskripsi;
    FirebaseUser currentUser;
    Dialog bottomSheetDialog;
    EditText et_password, et_newPassword, et_confirmPassword;
    Menu actionMenu;
    Context context;
    Date dateTime;
    DateFormat dateFormat;
    ArrayList<Nilai> nilaiArrayList = new ArrayList<>();
    ArrayList<Pembina> pembinaArrayList = new ArrayList<>();
    ArrayList<Siswa> siswaArrayList = new ArrayList<>();
    ArrayList<Absensi> absensiArrayList = new ArrayList<>();
    ArrayList<SubEkstrakurikuler> subEkskul = new ArrayList<>();
    MediaPlayer utama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_absensi, R.id.navigation_nilai,
                R.id.navigation_data_siswa, R.id.navigation_profile_pembina)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
        mFirestore = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        spotDialog = new SpotsDialog.Builder()
                .setContext(MainActivity.this)
                .setMessage("Please wait...")
                .setCancelable(false)
                .build();
        spotDialog.dismiss();
        initPopUp();
        setupPopupImage();
        initBottom();
        dataShow();
        showDataAbsensi();
        showDataPembina();
        showDataSiswa();
    }

    private void showDataPembina() {
        Query query = mFirestore.collection("pembina")
                .orderBy("timeStamp", Query.Direction.DESCENDING);
        query.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                if (queryDocumentSnapshots.isEmpty()) {
                    Log.e("Data Kosong", "");
                    return;
                } else {
                    List<Pembina> pembinas = queryDocumentSnapshots.toObjects(Pembina.class);
                    pembinaArrayList.addAll(pembinas);
                }
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("ERROR", e.getMessage());
                    }
                });

    }

    private void showDataSiswa() {
        Query query = mFirestore.collection("siswa")
                .orderBy("timeStamp", Query.Direction.DESCENDING);
        query.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                if (queryDocumentSnapshots.isEmpty()) {
                    Log.e("Data Kosong", "");
                    return;
                } else {
                    List<Siswa> siswas = queryDocumentSnapshots.toObjects(Siswa.class);
                    siswaArrayList.addAll(siswas);
                }
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("ERROR", e.getMessage());
                    }
                });

    }

    private void showDataAbsensi() {
        Query query = mFirestore.collection("absensi_ekskul")
                .orderBy("timeStamp", Query.Direction.DESCENDING);
        query.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                if (queryDocumentSnapshots.isEmpty()) {
                    Log.e("Data Kosong", "");
                    return;
                } else {
                    List<Absensi> absensis = queryDocumentSnapshots.toObjects(Absensi.class);
                    absensiArrayList.addAll(absensis);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("ERROR", e.getMessage());
            }
        });
    }

    private void dataShow() {
        Query query = mFirestore.collection("nilai_ekskul")
                .orderBy("timeStamp", Query.Direction.DESCENDING);
        query.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                if (queryDocumentSnapshots.isEmpty()) {
                    Log.e("Data Kosong", "");
                    return;
                } else {
                    List<Nilai> nilais = queryDocumentSnapshots.toObjects(Nilai.class);
                    nilaiArrayList.addAll(nilais);
                }
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("ERROR", e.getMessage());
                    }
                });

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void initBottom() {

        bottomSheetDialog = new Dialog(this);
        bottomSheetDialog.setContentView(R.layout.change_password);
        bottomSheetDialog.getWindow().setLayout(Toolbar.LayoutParams.MATCH_PARENT, Toolbar.LayoutParams.WRAP_CONTENT);
        bottomSheetDialog.getWindow().getAttributes().gravity = Gravity.TOP;

        et_password = bottomSheetDialog.findViewById(R.id.et_password);
        et_newPassword = bottomSheetDialog.findViewById(R.id.et_newPassword);
        et_confirmPassword = bottomSheetDialog.findViewById(R.id.et_confirmPassword);
        image_cancel = bottomSheetDialog.findViewById(R.id.image_cancel);
        btn_simpan = bottomSheetDialog.findViewById(R.id.btn_simpan);
        image_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.cancel();
            }
        });
        btn_simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String password = et_password.getText().toString().trim();
                String newPass = et_newPassword.getText().toString().trim();
                String confirmPass = et_confirmPassword.getText().toString().trim();
                if (password.isEmpty()) {
                    et_password.setError("Password is required!");
                    return;
                }
                if (newPass.isEmpty()) {
                    et_newPassword.setError("New assword is required!");
                    return;
                }
                if (confirmPass.isEmpty()) {
                    et_confirmPassword.setError("Confirm password is required!");
                    return;
                }
                if (password.length() < 4 && newPass.length() < 4 && confirmPass.length() < 4) {
                    et_password.setError("The password cannot be less than 4 characters!");
                    return;
                }
                if (!newPass.equals(confirmPass)) {
                    et_newPassword.setError("New password does not matches!");
                    return;
                }
                FirebaseUser user = mAuth.getCurrentUser();
                AuthCredential credential = EmailAuthProvider.getCredential(user.getEmail(), password);
                user.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            spotDialog.show();
                            user.updatePassword(newPass).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        spotDialog.dismiss();
                                        showMessage("Password Changed!");
                                        bottomSheetDialog.dismiss();
                                    } else {
                                        spotDialog.dismiss();
                                        bottomSheetDialog.dismiss();
                                        showMessage(task.getException().getMessage());
                                    }
                                }
                            });
                        } else {
                            spotDialog.dismiss();
                            bottomSheetDialog.dismiss();
                            showMessage(task.getException().getMessage());
                        }
                    }
                });
            }
        });
    }


    private void setupPopupImage() {
        image_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAndRequestPermission();
            }
        });
        image_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAndRequestPermission();
            }
        });
    }

    private void initPopUp() {
        popupAdd = new Dialog(this);
        popupAdd.setContentView(R.layout.popup);
        popupAdd.getWindow().setLayout(Toolbar.LayoutParams.MATCH_PARENT, Toolbar.LayoutParams.WRAP_CONTENT);
        popupAdd.getWindow().getAttributes().gravity = Gravity.TOP;

        image_post = popupAdd.findViewById(R.id.image_post);
        image_camera = popupAdd.findViewById(R.id.image_camera);
        et_title = popupAdd.findViewById(R.id.et_title);
        et_deskripsi = popupAdd.findViewById(R.id.et_deskripsi);
        btn_save = popupAdd.findViewById(R.id.btn_save);
        cancel = popupAdd.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupAdd.cancel();
            }
        });
        //load image user
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!et_title.getText().toString().isEmpty() && !et_deskripsi.getText().toString().isEmpty() && image_post != null) {
                    spotDialog.show();
                    StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("ekskul_image");
                    final StorageReference imageFilePath = storageReference.child(pickImage.getLastPathSegment());
                    imageFilePath.putFile(pickImage).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            imageFilePath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    spotDialog.show();
                                    String imageDownloadLink = uri.toString();
                                    //create post object
                                    Ekstrakurikuler post = new Ekstrakurikuler(et_title.getText().toString(),
                                            et_deskripsi.getText().toString(),
                                            imageDownloadLink,
                                            currentUser.getUid()
                                    );
                                    addPost(post);
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    spotDialog.dismiss();
                                    showMessage(e.getMessage());
                                }
                            });
                        }
                    });
                } else {
                    spotDialog.dismiss();
                    showMessage("Please verify all input and choose Post Image!");
                }
            }
        });
    }

    private void addPost(Ekstrakurikuler post) {
        FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        DocumentReference reference = firestore.collection("EKSKUL").document();
        String key = reference.getId();
        post.setId_ekskul(key);
        reference.set(post).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                showMessage("Successfully!");
                popupAdd.dismiss();
                spotDialog.dismiss();
            }
        });
    }

    private void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        actionMenu = menu;
        actionMenu.findItem(R.id.add).setVisible(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.report) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions((Activity) context,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 10);
            } else {
                createPdfNilai(Common.getAppPath(context) + "Laporan Nilai.pdf");
            }
        }
        if (id == R.id.absensi) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions((Activity) context,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 10);
            } else {
                createPdfAbsensi(Common.getAppPath(context) + "Laporan Presence.pdf");
            }
        }
        if (id == R.id.pembina) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions((Activity) context,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 10);
            } else {
                createPdfDataPembina(Common.getAppPath(context) + "Laporan Data Pembina.pdf");
            }
        }
        if (id == R.id.siswa) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions((Activity) context,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 10);
            } else {
                createPdfDataSiswa(Common.getAppPath(context) + "Laporan Data Siswa.pdf");
            }
        }

        if (id == R.id.add) {
            showAddDialog();
        }
        if (id == R.id.changePassword) {
            showDialogChange();
        }
        if (id == R.id.logout) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Exit Application");
            builder.setMessage("Do you want to exit this application?");
            builder.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    spotDialog.show();
                    FirebaseAuth.getInstance().signOut();
                    Intent i = new Intent(MainActivity.this, LoginActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
                }
            });
            builder.setPositiveButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    spotDialog.dismiss();
                    dialog.dismiss();
                }
            });
            builder.show();
        }
        return super.onOptionsItemSelected(item);
    }

    private void createPdfDataSiswa(String appPath) {
        if (new File(appPath).exists()) {
            new File(appPath).delete();
        }
        try {
            dateTime = new Date();
            Document document = new Document(PageSize.A4, 30, 30, 36, 100);
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(appPath));

//          cover header
            Rectangle rectangle = new Rectangle(30, 30, 610, 800);
            writer.setBoxSize("art", rectangle);
            HeaderFooterPageEvent event = new HeaderFooterPageEvent();
            writer.setPageEvent(event);

            document.open();
            document.addAuthor("SMK Daarul Uluum");
            document.addCreator("Muhamad Nur Sukur");
            document.addCreationDate();

            Font fontNormal = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL, BaseColor.BLACK);
            Font fontBold = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD, BaseColor.BLACK);
            Font fontBigBold = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD, BaseColor.BLACK);

            Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.logo);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            Image img = null;
            byte[] bytes = stream.toByteArray();
            try {
                img = Image.getInstance(bytes);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            img.scaleAbsolute(60, 60);
            document.add(img);

            addLineSeparator(document);

            Paragraph paragraphTitle = new Paragraph("Data Siswa", fontBigBold);
            paragraphTitle.setAlignment(Element.ALIGN_CENTER);
            paragraphTitle.setSpacingAfter(20);
            document.add(paragraphTitle);

            PdfPTable table = new PdfPTable(7);
            table.setWidthPercentage(100);
            table.setWidths(new float[]{1, 2, 3, 3, 2, 2, 4});
            PdfPCell cell;
            cell = new PdfPCell(new Phrase("No.", fontBold));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("NISN", fontBold));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Nama", fontBold));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Email", fontBold));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Kelas", fontBold));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Telephone", fontBold));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Alamat", fontBold));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            table.addCell(cell);

            for (int i = 0; i < siswaArrayList.size(); i++) {
                cell = new PdfPCell(new Phrase(String.valueOf(1 + i), fontNormal));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(siswaArrayList.get(i).getNisn()), fontNormal));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(siswaArrayList.get(i).getName(), fontNormal));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(siswaArrayList.get(i).getEmail(), fontNormal));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(siswaArrayList.get(i).getKelas(), fontNormal));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(siswaArrayList.get(i).getPhone()), fontNormal));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(String.valueOf(siswaArrayList.get(i).getAlamat()), fontNormal));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

            }

            document.add(table);

            Paragraph paragraph2 = new Paragraph("", fontNormal);
            paragraph2.setAlignment(Element.ALIGN_RIGHT);
            paragraph2.setSpacingAfter(50);
            document.add(paragraph2);
            addLineSpace(document);
            addLineSpace(document);

            final PdfPTable table1 = new PdfPTable(1);
            table1.setWidthPercentage(30);
            table1.setFooterRows(100);
            table1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table1.setWidths(new float[]{5});
            dateFormat = new SimpleDateFormat("dd MMMM YYYY");
            PdfPCell cell1;
            cell1 = new PdfPCell(new Phrase("Jakarta, " + dateFormat.format(dateTime), fontNormal));
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell1.setBorder(Rectangle.NO_BORDER);
            table1.addCell(cell1);

            Common.openFilePdf(context, new File(Common.getAppPath(context) + "Laporan Data Siswa.pdf"));
            document.close();
            Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();

        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
            Log.e("GAGAL", e.getMessage());
        }
    }

    private void createPdfDataPembina(String appPath) {
        if (new File(appPath).exists()) {
            new File(appPath).delete();
        }
        try {
            dateTime = new Date();
            Document document = new Document(PageSize.A4, 30, 30, 36, 100);
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(appPath));

//          cover header
            Rectangle rectangle = new Rectangle(30, 30, 610, 800);
            writer.setBoxSize("art", rectangle);
            HeaderFooterPageEvent event = new HeaderFooterPageEvent();
            writer.setPageEvent(event);

            document.open();
            document.addAuthor("SMK Daarul Uluum");
            document.addCreator("Muhamad Nur Sukur");
            document.addCreationDate();

            Font fontNormal = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.NORMAL, BaseColor.BLACK);
            Font fontBold = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLD, BaseColor.BLACK);
            Font fontBigBold = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD, BaseColor.BLACK);

            Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.logo);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            Image img = null;
            byte[] bytes = stream.toByteArray();
            try {
                img = Image.getInstance(bytes);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            img.scaleAbsolute(60, 60);
            document.add(img);

            addLineSeparator(document);

            Paragraph paragraphTitle = new Paragraph("Data Pembina", fontBigBold);
            paragraphTitle.setAlignment(Element.ALIGN_CENTER);
            paragraphTitle.setSpacingAfter(20);
            document.add(paragraphTitle);

            PdfPTable table = new PdfPTable(6);
            table.setWidthPercentage(100);
            table.setWidths(new float[]{1, 3, 3, 2, 2, 4});
            PdfPCell cell;
            cell = new PdfPCell(new Phrase("No.", fontBold));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("Nama", fontBold));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Email", fontBold));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Ekstrakurikuler", fontBold));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Telephone", fontBold));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Alamat", fontBold));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            table.addCell(cell);

            for (int i = 0; i < pembinaArrayList.size(); i++) {
                cell = new PdfPCell(new Phrase(String.valueOf(1 + i), fontNormal));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(pembinaArrayList.get(i).getName()), fontNormal));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(pembinaArrayList.get(i).getEmail(), fontNormal));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(pembinaArrayList.get(i).getEkskul(), fontNormal));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(pembinaArrayList.get(i).getPhone()), fontNormal));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(String.valueOf(pembinaArrayList.get(i).getAlamat()), fontNormal));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);
            }

            document.add(table);

            Paragraph paragraph2 = new Paragraph("", fontNormal);
            paragraph2.setAlignment(Element.ALIGN_RIGHT);
            paragraph2.setSpacingAfter(50);
            document.add(paragraph2);
            addLineSpace(document);
            addLineSpace(document);

            final PdfPTable table1 = new PdfPTable(1);
            table1.setWidthPercentage(30);
            table1.setFooterRows(100);
            table1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table1.setWidths(new float[]{5});
            dateFormat = new SimpleDateFormat("dd MMMM YYYY");
            PdfPCell cell1;
            cell1 = new PdfPCell(new Phrase("Jakarta, " + dateFormat.format(dateTime), fontNormal));
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell1.setBorder(Rectangle.NO_BORDER);
            table1.addCell(cell1);

            Common.openFilePdf(context, new File(Common.getAppPath(context) + "Laporan Data Pembina.pdf"));
            document.close();
            Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();

        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
            Log.e("GAGAL", e.getMessage());
        }

    }

    private void createPdfNilai(String appPath) {
        if (new File(appPath).exists()) {
            new File(appPath).delete();
        }
        try {
            dateTime = new Date();
            Document document = new Document(PageSize.A4, 30, 30, 36, 100);
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(appPath));

//          cover header
            Rectangle rectangle = new Rectangle(30, 30, 610, 800);
            writer.setBoxSize("art", rectangle);
            HeaderFooterPageEvent event = new HeaderFooterPageEvent();
            writer.setPageEvent(event);

            document.open();
            document.addAuthor("SMK Daarul Uluum");
            document.addCreator("Muhamad Nur Sukur");
            document.addCreationDate();

            Font fontNormal = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL, BaseColor.BLACK);
            Font fontBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD, BaseColor.BLACK);
            Font fontBigBold = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD, BaseColor.BLACK);

            Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.logo);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            Image img = null;
            byte[] bytes = stream.toByteArray();
            try {
                img = Image.getInstance(bytes);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            img.scaleAbsolute(60, 60);
            document.add(img);

            addLineSeparator(document);

            Paragraph paragraphTitle = new Paragraph("Data Nilai Ekstrakurikuler", fontBigBold);
            paragraphTitle.setAlignment(Element.ALIGN_CENTER);
            paragraphTitle.setSpacingAfter(20);
            document.add(paragraphTitle);

            PdfPTable table = new PdfPTable(6);
            table.setWidthPercentage(100);
            table.setWidths(new float[]{1, 3, 5, 3, 2, 2});
            PdfPCell cell;
            cell = new PdfPCell(new Phrase("No.", fontBold));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("Tanggal", fontBold));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Nama", fontBold));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Ekstrakurikuler", fontBold));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Nilai", fontBold));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Grade", fontBold));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            table.addCell(cell);

            for (int i = 0; i < nilaiArrayList.size(); i++) {
                cell = new PdfPCell(new Phrase(String.valueOf(1 + i), fontNormal));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(nilaiArrayList.get(i).getTimeStamp().toLocaleString()), fontNormal));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(nilaiArrayList.get(i).getName(), fontNormal));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(nilaiArrayList.get(i).getEkskul(), fontNormal));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(nilaiArrayList.get(i).getNilai()), fontNormal));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);

                int nilai = Integer.parseInt(String.valueOf(nilaiArrayList.get(i).getNilai()));
                if (nilai >= 80) {
                    cell = new PdfPCell(new Phrase("A", fontNormal));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);
                } else if (nilai >= 70) {
                    cell = new PdfPCell(new Phrase("B", fontNormal));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);
                } else if (nilai >= 60) {
                    cell = new PdfPCell(new Phrase("C", fontNormal));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);
                } else if (nilai >= 50) {
                    cell = new PdfPCell(new Phrase("D", fontNormal));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);
                } else {
                    cell = new PdfPCell(new Phrase("E", fontNormal));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);
                }


            }

            document.add(table);

            Paragraph paragraph2 = new Paragraph("", fontNormal);
            paragraph2.setAlignment(Element.ALIGN_RIGHT);
            paragraph2.setSpacingAfter(50);
            document.add(paragraph2);
            addLineSpace(document);
            addLineSpace(document);

            final PdfPTable table1 = new PdfPTable(1);
            table1.setWidthPercentage(30);
            table1.setFooterRows(100);
            table1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table1.setWidths(new float[]{5});
            dateFormat = new SimpleDateFormat("dd MMMM YYYY");
            PdfPCell cell1;
            cell1 = new PdfPCell(new Phrase("Jakarta, " + dateFormat.format(dateTime), fontNormal));
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell1.setBorder(Rectangle.NO_BORDER);
            table1.addCell(cell1);

            Common.openFilePdf(context, new File(Common.getAppPath(context) + "Laporan Nilai.pdf"));
            document.close();
            Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();

        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
            Log.e("GAGAL", e.getMessage());
        }
    }

    private void createPdfAbsensi(String appPath) {
        dateTime = new Date();
        if (new File(appPath).exists()) {
            new File(appPath).delete();
        }

        try {
            Document document = new Document(PageSize.A4, 30, 30, 36, 100);
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(appPath));

//          cover header
            Rectangle rectangle = new Rectangle(30, 30, 610, 800);
            writer.setBoxSize("art", rectangle);
            HeaderFooterPageEvent event = new HeaderFooterPageEvent();
            writer.setPageEvent(event);

            document.open();
            document.addAuthor("SMK Daarul Uluum");
            document.addCreator("Muhamad Nur Sukur");
            document.addCreationDate();

            Font fontNormal = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL, BaseColor.BLACK);
            Font fontBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD, BaseColor.BLACK);
            Font fontBigBold = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD, BaseColor.BLACK);

            Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.logo);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            Image img = null;
            byte[] bytes = stream.toByteArray();
            try {
                img = Image.getInstance(bytes);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            img.scaleAbsolute(60, 60);
            document.add(img);

            addLineSeparator(document);

            Paragraph paragraphTitle = new Paragraph("Data Presence Ekstrakurikuler", fontBigBold);
            paragraphTitle.setAlignment(Element.ALIGN_CENTER);
            paragraphTitle.setSpacingAfter(20);
            document.add(paragraphTitle);

            PdfPTable table = new PdfPTable(5);
            table.setWidthPercentage(100);
            table.setWidths(new float[]{1, 3, 5, 3, 2});
            PdfPCell cell;
            cell = new PdfPCell(new Phrase("No.", fontBold));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("Tanggal", fontBold));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Nama", fontBold));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            table.addCell(cell);


            cell = new PdfPCell(new Phrase("Ekstrakurikuler", fontBold));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Kehadiran", fontBold));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            table.addCell(cell);
            dateFormat = new SimpleDateFormat("dd MMMM YYYY");
            for (int i = 0; i < absensiArrayList.size(); i++) {
                cell = new PdfPCell(new Phrase(String.valueOf(1 + i), fontNormal));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(absensiArrayList.get(i).getTimeStamp().toLocaleString()), fontNormal));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(absensiArrayList.get(i).getName(), fontNormal));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(absensiArrayList.get(i).getEkskul(), fontNormal));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(absensiArrayList.get(i).getKehadiran()), fontNormal));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);

            }

            document.add(table);

            Common.openFilePdf(context, new File(Common.getAppPath(context) + "Laporan Presence.pdf"));
            document.close();
            Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();

        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
            Log.e("GAGAL", e.getMessage());
        }
    }

    private void addLineSeparator(Document document) throws DocumentException {
        LineSeparator lineSeparator = new LineSeparator();
        lineSeparator.setLineColor(new BaseColor(0, 0, 0, 68));
        addLineSpace(document);
        document.add(new Chunk(lineSeparator));
        addLineSpace(document);
    }

    private void addLineSpace(Document document) throws DocumentException {
        document.add(new Paragraph(""));
    }

    private void showDialogChange() {
        bottomSheetDialog.show();
    }

    private void showAddDialog() {
        popupAdd.show();
    }

    //    Exit Application
    boolean isBackButtonClicked = false;

    @Override
    public void onBackPressed() {
        if (isBackButtonClicked) {
            super.onBackPressed();
            return;
        }
        this.isBackButtonClicked = false;
//        Toast.makeText(this,"Please click BACK again to exit!",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPostResume() {
//        mAuth.signOut();
        isBackButtonClicked = false;
        super.onPostResume();
    }

    private void checkAndRequestPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Toast.makeText(this, "Please accept for required permission", Toast.LENGTH_SHORT).show();
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        PreqCode);
            }
        } else {
            openGallery();
        }
    }

    private void openGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, REQUESTCODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUESTCODE && data != null) {
            pickImage = data.getData();
            image_post.setImageURI(pickImage);
        }
    }
}