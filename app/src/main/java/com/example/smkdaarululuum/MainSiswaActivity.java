package com.example.smkdaarululuum;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import dmax.dialog.SpotsDialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.example.smkdaarululuum.Interface.ItemClickListener;
import com.example.smkdaarululuum.activity.auth.LoginActivity;
import com.example.smkdaarululuum.model.DataSiswaEkskul;
import com.example.smkdaarululuum.model.Ekstrakurikuler;
import com.example.smkdaarululuum.model.Siswa;
import com.example.smkdaarululuum.ui.home.HomeFragment;
import com.example.smkdaarululuum.ui.siswa.AbsensiSiswaFragment;
import com.example.smkdaarululuum.ui.siswa.NilaiSiswaFragment;
import com.example.smkdaarululuum.ui.siswa.ProfileSiswaFragment;
import com.example.smkdaarululuum.viewholder.DaftarEksulViewHolder;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

public class MainSiswaActivity extends AppCompatActivity {
    Menu actionMenu;
    AlertDialog spotDialog;
    FirebaseAuth mAuth;
    FirebaseFirestore mStore;
    FirebaseUser currentUser;
    Dialog popUpDaftar,popupChange;
    DocumentReference mReference;
    RecyclerView recycler_daftar;
    FirestoreRecyclerAdapter adapter;
    BottomSheetDialog bottomSheetDialog;
    ImageView image_close, cancel,image_cancel;
    Button btn_daftar,btn_simpan;
    TextView tv_name, tv_date, tv_nisn, tv_kelas, tv_title, tv_nama,tv_idEkskul;
    EditText et_password,et_newPassword,et_confirmPassword;
    String userID= "";
    Siswa siswa;
    String ekskul_id = "";
    ProgressBar progressBar;
    MediaPlayer utama, nilai, absen, profile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_siswa);
        mAuth = FirebaseAuth.getInstance();
        userID = mAuth.getCurrentUser().getUid();
        mStore = FirebaseFirestore.getInstance();
        BottomNavigationView bottomNavigationView = findViewById(R.id.nav_view);
        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
        loadFragment(new HomeFragment());
//        utama = MediaPlayer.create(this, R.raw.utama);
        nilai = MediaPlayer.create(this, R.raw.nilai);
        absen = MediaPlayer.create(this, R.raw.absen);
        profile = MediaPlayer.create(this, R.raw.profile);
        spotDialog = new SpotsDialog.Builder()
                .setContext(getApplicationContext())
                .setMessage("Please wait...")
                .setCancelable(false)
                .build();
        spotDialog.dismiss();
        mStore = FirebaseFirestore.getInstance();
        mReference = mStore.collection("siswa").document(userID);
        mReference.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                siswa = documentSnapshot.toObject(Siswa.class);
                tv_name.setText(siswa.getName());
                tv_nama.setText(siswa.getName());
                tv_nisn.setText(siswa.getNisn());
                tv_kelas.setText(siswa.getKelas());
            }
        });
        initPopup();
        loadDataEkskul();
        popUpDaftar();
        changePassword();
    }

    private void changePassword() {
        popupChange = new Dialog(this);
        popupChange.setContentView(R.layout.change_password);
        popupChange.getWindow().setLayout(Toolbar.LayoutParams.MATCH_PARENT,Toolbar.LayoutParams.WRAP_CONTENT);
        popupChange.getWindow().getAttributes().gravity = Gravity.TOP;

//        image_user = popupAdd.findViewById(R.id.image_user);
        et_password = popupChange.findViewById(R.id.et_password);
        et_newPassword = popupChange.findViewById(R.id.et_newPassword);
        et_confirmPassword = popupChange.findViewById(R.id.et_confirmPassword);
        image_cancel = popupChange.findViewById(R.id.image_cancel);
        btn_simpan = popupChange.findViewById(R.id.btn_simpan);
        image_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupChange.cancel();
            }
        });
        btn_simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String password = et_password.getText().toString().trim();
                String newPass = et_newPassword.getText().toString().trim();
                String confirmPass = et_confirmPassword.getText().toString().trim();
                if (password.isEmpty()){
                    et_password.setError("Password is required!");
                    return;
                }
                if(newPass.isEmpty()){
                    et_newPassword.setError("New assword is required!");
                    return;
                }
                if (confirmPass.isEmpty()){
                    et_confirmPassword.setError("Confirm password is required!");
                    return;
                }
                if (password.length() <4 && newPass.length() <4 && confirmPass.length() <4){
                    et_password.setError("The password cannot be less than 4 characters!");
                    return;
                }
                if (!newPass.equals(confirmPass)){
                    et_newPassword.setError("New password does not matches!");
                    return;
                }
                FirebaseUser user = mAuth.getCurrentUser();
                AuthCredential credential = EmailAuthProvider.getCredential(user.getEmail(),password);
                user.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            spotDialog.show();
                            user.updatePassword(newPass).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()){
                                        spotDialog.dismiss();
                                        showMessage("Password Changed!");
                                        popupChange.dismiss();
                                    }else {
                                        spotDialog.dismiss();
                                        popupChange.dismiss();
                                        showMessage(task.getException().getMessage());
                                    }
                                }
                            });
                        }else {
                            spotDialog.dismiss();
                            popupChange.dismiss();
                            showMessage(task.getException().getMessage());
                        }
                    }
                });
            }
        });
    }

    private void popUpDaftar() {
        popUpDaftar = new Dialog(this);
        popUpDaftar.setContentView(R.layout.daftar_ekskul);
        popUpDaftar.getWindow().setLayout(Toolbar.LayoutParams.MATCH_PARENT,Toolbar.LayoutParams.WRAP_CONTENT);
        popUpDaftar.getWindow().getAttributes().gravity = Gravity.TOP;
        tv_name = popUpDaftar.findViewById(R.id.tv_name);
        tv_title = popUpDaftar.findViewById(R.id.tv_title);
        tv_nisn = popUpDaftar.findViewById(R.id.tv_nisn);
        tv_kelas = popUpDaftar.findViewById(R.id.tv_kelas);
        tv_idEkskul = popUpDaftar.findViewById(R.id.tv_idEkskul);
        cancel = popUpDaftar.findViewById(R.id.cancel);
        btn_daftar = popUpDaftar.findViewById(R.id.btn_daftar);
        progressBar = popUpDaftar.findViewById(R.id.progressBar);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpDaftar.cancel();
            }
        });
        btn_daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nama = tv_name.getText().toString().trim();
                String nisn = tv_nisn.getText().toString().trim();
                String kelas = tv_kelas.getText().toString().trim();
                String title = tv_title.getText().toString().trim();
                ekskul_id = tv_idEkskul.getText().toString().trim();
                if (!nama.isEmpty() && !nisn.isEmpty() && !kelas.isEmpty() && !title.isEmpty()){
                    progressBar.setVisibility(View.VISIBLE);
                    btn_daftar.setVisibility(View.GONE);
                    DataSiswaEkskul dataSiswaEkskul = new DataSiswaEkskul(nama
                            ,nisn,kelas,title,mAuth.getCurrentUser().getUid(),ekskul_id);
                    addData(dataSiswaEkskul);
                }else {
                    showMessage("failed new data");
                }

            }
        });
    }

    private void initPopup() {
        bottomSheetDialog = new BottomSheetDialog(this);
        bottomSheetDialog.setContentView(R.layout.add_daftar_ekskul);
        bottomSheetDialog.getWindow().setLayout(Toolbar.LayoutParams.MATCH_PARENT,Toolbar.LayoutParams.WRAP_CONTENT);
        bottomSheetDialog.getWindow().getAttributes().gravity = Gravity.TOP;
        recycler_daftar = bottomSheetDialog.findViewById(R.id.recyclerView);
        recycler_daftar.setLayoutManager(new LinearLayoutManager(getApplicationContext(),
                LinearLayoutManager.VERTICAL,false));
        tv_nama = bottomSheetDialog.findViewById(R.id.tv_name);
        image_close = bottomSheetDialog.findViewById(R.id.image_close);
        image_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.cancel();
            }
        });

    }

    private void loadDataEkskul() {
        Query query = FirebaseFirestore.getInstance().collection("EKSKUL").
                orderBy("timeStamp", Query.Direction.ASCENDING);
        FirestoreRecyclerOptions<Ekstrakurikuler> options = new FirestoreRecyclerOptions.Builder<Ekstrakurikuler>().
                setQuery(query, Ekstrakurikuler.class)
                .build();
        adapter = new FirestoreRecyclerAdapter<Ekstrakurikuler, DaftarEksulViewHolder>(options) {

            @NonNull
            @Override
            public DaftarEksulViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {
                View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_daftar_ekskul,group,false);
                return new DaftarEksulViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull DaftarEksulViewHolder holder, int position, @NonNull Ekstrakurikuler model) {
                holder.textView.setText(model.getTitle());
                Glide.with(getApplicationContext()).load(model.getImage_ekskul()).into(holder.circleImageView);
                holder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View v, int adapterPosition, boolean b) {
                        showMessage(model.getTitle());
                        tv_title.setText(model.getTitle());
                        tv_idEkskul.setText(model.getId_ekskul());
                        addDaftarEkskul();
                    }
                });
            }
        };
        recycler_daftar.setAdapter(adapter);
    }

    private void addDaftarEkskul() {
        popUpDaftar.show();
        progressBar.setVisibility(View.GONE);
        btn_daftar.setVisibility(View.VISIBLE);
    }


    private void addData(DataSiswaEkskul dataSiswaEkskul) {
        mStore = FirebaseFirestore.getInstance();
        mReference = mStore.collection("daftarEkskul").document(userID+ekskul_id);
        String key = mReference.getId();
        dataSiswaEkskul.setKey(key);
        mReference.set(dataSiswaEkskul).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                showMessage("success");
                popUpDaftar.dismiss();
                bottomSheetDialog.dismiss();

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                showMessage(e.getMessage());
            }
        });
    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.nav_host_fragment, fragment)
                    .addToBackStack(null)
                    .commit();
            return true;
        }
        return false;
    }
    private BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment = null;
            switch (item.getItemId()){
                case R.id.navigation_home_siswa:
                    fragment = new HomeFragment();
                    break;
                case R.id.navigation_absensi_siswa:
                    fragment = new AbsensiSiswaFragment();
                    absen.start();
                    break;
                case R.id.navigation_nilai_siswa:
                    fragment = new NilaiSiswaFragment();
                    nilai.start();
                    break;
                case R.id.navigation_profile_siswa:
                    fragment = new ProfileSiswaFragment();
                    profile.start();
                    break;
            }
            return loadFragment(fragment);
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_siswa,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.daftar_ekskul){
            daftarEkskul();
        }
        if (id == R.id.changePassword){
            showDialogChange();
        }
        if (id == R.id.logout){
            mAuth.signOut();
            startActivity(new Intent(getApplicationContext(),LoginActivity.class));
            showMessage("Log Out Success!");
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDialogChange() {
        popupChange.show();
    }

    private void daftarEkskul() {
        bottomSheetDialog.show();
    }

    private void showMessage(String message) {
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }
    boolean isBackButtonClicked = false;

    @Override
    public void onBackPressed() {
        if (isBackButtonClicked){
            super.onBackPressed();
            return;
        }
        this.isBackButtonClicked = false;
//        Toast.makeText(this,"Please click BACK again to exit!",Toast.LENGTH_SHORT).show();
    }
    @Override
    protected void onPostResume() {
//        mAuth.signOut();
        isBackButtonClicked = false;
        super.onPostResume();
    }
}
