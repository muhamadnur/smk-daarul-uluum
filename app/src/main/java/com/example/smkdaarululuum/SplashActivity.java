package com.example.smkdaarululuum;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.smkdaarululuum.activity.auth.LoginActivity;

public class SplashActivity extends AppCompatActivity {

    private TextView tv;
    private ImageView imageView;
    MediaPlayer selamat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        tv = findViewById(R.id.tv_wellcome);
        imageView = findViewById(R.id.image_logo);
        Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.my_transition);
        tv.startAnimation(myAnim);
        selamat = MediaPlayer.create(this, R.raw.selamat_datang);
        selamat.start();
        imageView.startAnimation(myAnim);
        final Intent i = new Intent(this, LoginActivity.class);
        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    startActivity(i);
                    finish();
                }
            }
        };
        timer.start();
    }
}
