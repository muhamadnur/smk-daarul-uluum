package com.example.smkdaarululuum.activity.auth;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import dmax.dialog.SpotsDialog;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smkdaarululuum.Common;
import com.example.smkdaarululuum.MainActivity;
import com.example.smkdaarululuum.MainSiswaActivity;
import com.example.smkdaarululuum.R;
import com.example.smkdaarululuum.model.Users;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class LoginActivity extends AppCompatActivity {
    EditText et_email,et_password;
    CircularProgressButton cirLoginButton;
    AlertDialog spotDialog;
    TextView tv_forgot;
    String userID;
    private FirebaseAuth mAuth;
    private Intent MainActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mAuth = FirebaseAuth.getInstance();
        spotDialog = new SpotsDialog.Builder()
                .setContext(LoginActivity.this)
                .setMessage("Please wait...")
                .setCancelable(false)
                .build();
        spotDialog.dismiss();
        MainActivity = new Intent(this, MainActivity.class);
        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        tv_forgot = findViewById(R.id.tv_forgot);
        cirLoginButton = findViewById(R.id.cirLoginButton);
        cirLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = et_email.getText().toString();
                final String password = et_password.getText().toString();
                if (email.isEmpty() || password.isEmpty()){
                    showMessage("Please verify all field!");
                }else {
                    spotDialog.show();
                    signIn(email,password);
                }
            }
        });
        tv_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText resetPassword = new EditText(v.getContext());
                final AlertDialog.Builder passwordResetDialog = new AlertDialog.Builder(v.getContext());
                passwordResetDialog.setTitle("Reset Password?");
                passwordResetDialog.setMessage("Enter your mail to received reset Link.");
                passwordResetDialog.setView(resetPassword);
                passwordResetDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String mail = resetPassword.getText().toString();
                        mAuth.sendPasswordResetEmail(mail).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                showMessage("Reset Link Sent To Your Email");
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                showMessage(e.getMessage());
                            }
                        });
                    }
                });
                passwordResetDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                passwordResetDialog.create().show();
            }
        });
    }

    private void signIn(String email, String password) {
            mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    final FirebaseUser user = mAuth.getCurrentUser();
                    if (task.isSuccessful()) {
                        checkLevelUser();
//                        if (!user.isEmailVerified()){
//                            spotDialog.dismiss();
//                            showMessage("Please verify your email!");
//                        }else {
//                            spotDialog.show();
//                            showMessage("Login Success!");
//                            updateUI();
//                        }
                    }else{
                        spotDialog.dismiss();
                        showMessage(task.getException().getMessage());
                    }
                }
            });
    }

    private void checkLevelUser() {
        FirebaseFirestore mStore = FirebaseFirestore.getInstance();
        userID = mAuth.getCurrentUser().getUid();
        DocumentReference mReference = mStore.collection("users").document(userID);
        mReference.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                Users users = documentSnapshot.toObject(Users.class);
                String level = users.getLevel();
                if (level.equals("Siswa")){
                    showMessage("Login success!");
                    startActivity(new Intent(getApplicationContext(), MainSiswaActivity.class));
                }
                if (level.equals("Pembina")){
                    showMessage("Login success!");
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                showMessage(e.getMessage());
            }
        });
    }

    private void showMessage(String message) {
        Toast.makeText(LoginActivity.this,message,Toast.LENGTH_SHORT).show();
    }
    public void onLoginClick(View View){
        startActivity(new Intent(this, RegistActivity.class));
        overridePendingTransition(R.anim.slide_in_right,R.anim.stay);
    }
    //Exit Application
    boolean isBackButtonClicked = false;

    @Override
    public void onBackPressed() {
        if (isBackButtonClicked){
            super.onBackPressed();
            return;
        }
        this.isBackButtonClicked = true;
        Toast.makeText(this,"Please click BACK again to exit!",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPostResume() {
        spotDialog.dismiss();
        isBackButtonClicked = false;
        super.onPostResume();
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//        FirebaseUser user = mAuth.getCurrentUser();
//        if (user != null) {
////            if (!user.isEmailVerified()){
////                spotDialog.dismiss();
////                showMessage("Please verify your email!");
////            }else {
////                spotDialog.show();
////                showMessage("Login Success!");
////            }
//            checkLevelUser();
//        }
//    }

//    @Override
//    protected void onStop() {
//        super.onStop();
//
//    }
}
