package com.example.smkdaarululuum.activity.auth;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import de.hdodenhof.circleimageview.CircleImageView;
import dmax.dialog.SpotsDialog;

import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smkdaarululuum.R;
import com.example.smkdaarululuum.model.Ekstrakurikuler;
import com.example.smkdaarululuum.model.Pembina;
import com.example.smkdaarululuum.model.Siswa;
import com.example.smkdaarululuum.model.Users;
import com.example.smkdaarululuum.viewholder.ItemEkskulViewHolder;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RegistActivity extends AppCompatActivity {
    EditText et_name,et_email,et_phone,et_password,et_alamat,et_kelas,et_nisn;
    CircularProgressButton cirRegisterButton;
    RadioGroup radioGroup, radio_eksul;
    TextInputLayout editTextKelas,editTextNisn;
    RadioButton radioButton,radioEkskul;
    AlertDialog dialog;
    FirebaseFirestore mStore;
    FirebaseAuth mAuth;
    DocumentReference mReference;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regist);
        mAuth = FirebaseAuth.getInstance();
        mStore = FirebaseFirestore.getInstance();
        dialog = new SpotsDialog.Builder()
                .setContext(RegistActivity.this)
                .setMessage("Please wait...")
                .setCancelable(false)
                .build();
        dialog.dismiss();
        et_name = findViewById(R.id.et_name);
        et_email = findViewById(R.id.et_email);
        et_phone = findViewById(R.id.et_phone);
        et_password = findViewById(R.id.et_password);
        et_alamat = findViewById(R.id.et_alamat);
        et_kelas = findViewById(R.id.et_kelas);
        et_nisn = findViewById(R.id.et_nisn);
        radioGroup = findViewById(R.id.radioGroup);
        radio_eksul = findViewById(R.id.radio_ekskul);
        linearLayout = findViewById(R.id.linearLayout);
        linearLayout.setVisibility(View.GONE);
        editTextKelas = findViewById(R.id.editTextKelas);
        editTextNisn = findViewById(R.id.editTextNisn);
        cirRegisterButton = findViewById(R.id.cirRegisterButton);
        cirRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
<<<<<<< HEAD
                FirebaseDatabase database = FirebaseDatabase.getInstance();
//                DatabaseReference ChekUser = database.getReference("users");
                createUserAccount(name,email,password,phone,alamat);
=======
                createUser();
>>>>>>> 141869fec5ddf50a0622eec73b4157228ab641e5
            }
        });

    }


    private void createUser() {
        int selectedID = radioGroup.getCheckedRadioButtonId();
        radioButton =findViewById(selectedID);
        String email = et_email.getText().toString().trim();
        String password = et_password.getText().toString().trim();
        String phone = et_phone.getText().toString().trim();
        String name = et_name.getText().toString().trim();
        String kelas = et_kelas.getText().toString().trim();
        String alamat = et_alamat.getText().toString().trim();
        String radio = radioButton.getText().toString();
        String nisn = et_nisn.getText().toString().trim();
        if (!email.isEmpty() && !password.isEmpty() && !phone.isEmpty() && !name.isEmpty()
                && !alamat.isEmpty() && !radio.isEmpty()){
            createUserAccount(email,name,password,phone,kelas,alamat,nisn,radio);
        }else {
            dialog.dismiss();
            showMessage("Field is required");
        }

    }

    private void createUserAccount(String email, String name, String password, String phone, String kelas,
                                   String alamat, String nisn, String radio) {
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    if (radio.equals("Siswa")){
                        Siswa siswa = new Siswa(nisn,name,email,kelas,phone,alamat,mAuth.getCurrentUser().getUid());
                        addDataSiswa(siswa);
                    }
                    if (radio.equals("Pembina")){
                        int selectEkskul = radio_eksul.getCheckedRadioButtonId();
                        radioEkskul =findViewById(selectEkskul);
                        String ekskul = radioEkskul.getText().toString();
                        Pembina pembina = new Pembina(name,email,phone,alamat,mAuth.getCurrentUser().getUid(),ekskul);
                        addDataPembina(pembina);
                    }
                    Users users = new Users(mAuth.getCurrentUser().getUid(),radio,email);
                    addUsers(users);
                }else {
                    showMessage(task.getException().getMessage());
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                showMessage(e.getMessage());
            }
        });
    }

<<<<<<< HEAD
    private void updateUserInfo(String name, String phone, String alamat, FirebaseUser currentUser) {
        DocumentReference documentReference = mStore.collection("users").document();
        Map<String,Object> user = new HashMap<>();
        user.put("id",currentUser.getUid());
        user.put("name",name);
        user.put("phone",phone);
        user.put("alamat",alamat);
        documentReference.set(user).addOnSuccessListener(new OnSuccessListener<Void>() {
=======
    public String onRadioButtonClicked(View view){
        boolean checked = ((RadioButton)view).isChecked();
        switch (view.getId()){
            case R.id.r_siswa:
                if (checked)
                    editTextKelas.setVisibility(View.VISIBLE);
                    et_kelas.setVisibility(View.VISIBLE);
                    editTextNisn.setVisibility(View.VISIBLE);
                    et_nisn.setVisibility(View.VISIBLE);
                    linearLayout.setVisibility(View.GONE);
                break;
            case R.id.r_pembina:
                if (checked)
                    linearLayout.setVisibility(View.VISIBLE);
                    editTextKelas.setVisibility(View.GONE);
                    et_kelas.setVisibility(View.GONE);
                    editTextNisn.setVisibility(View.GONE);
                    et_nisn.setVisibility(View.GONE);
                break;
        }
        return null;
    }


    private void addUsers(Users users) {
        mStore = FirebaseFirestore.getInstance();
        mReference = mStore.collection("users").document(mAuth.getCurrentUser().getUid());
        mReference.set(users).addOnSuccessListener(new OnSuccessListener<Void>() {
>>>>>>> 141869fec5ddf50a0622eec73b4157228ab641e5
            @Override
            public void onSuccess(Void aVoid) {
                showMessage("Created Users!");
                startActivity(new Intent(getApplicationContext(),LoginActivity.class));
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                showMessage(e.getMessage());
            }
        });
    }

    private void addDataPembina(Pembina pembina) {
        mStore = FirebaseFirestore.getInstance();
        mReference = mStore.collection("pembina").document(mAuth.getCurrentUser().getUid());
        String key = mReference.getId();
        pembina.setId_pembina(key);
        mReference.set(pembina).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                showMessage("Created Data Pembina");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                showMessage(e.getMessage());
            }
        });
    }

    private void addDataSiswa(Siswa siswa) {
        mStore = FirebaseFirestore.getInstance();
        mReference = mStore.collection("siswa").document(mAuth.getCurrentUser().getUid());
        String key = mReference.getId();
        siswa.setId_siswa(key);
        mReference.set(siswa).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                showMessage("Created Data Siswa!");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                showMessage(e.getMessage());
            }
        });

    }

    private void showMessage(String message) {
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }

    public void onLoginClick(View view) {
        startActivity(new Intent(this, LoginActivity.class));
        overridePendingTransition(R.anim.slide_in_left,android.R.anim.slide_out_right);
    }
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(Color.TRANSPARENT);
            window.setStatusBarColor(getResources().getColor(R.color.register_bk_color));
        }
    }
}
