package com.example.smkdaarululuum.activity.sub_ekstrakurikuler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import dmax.dialog.SpotsDialog;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.example.smkdaarululuum.Interface.ItemClickListener;
import com.example.smkdaarululuum.R;
import com.example.smkdaarululuum.model.DataSiswaEkskul;
import com.example.smkdaarululuum.model.SubEkstrakurikuler;
import com.example.smkdaarululuum.viewholder.HomeViewHolder;
import com.example.smkdaarululuum.viewholder.SubViewHolder;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;


public class SubEkskulActivity extends AppCompatActivity {
    static int PreqCode = 3;
    static int REQUESTCODE = 3;
    Menu actionMenu;
    AlertDialog spotDialog;
    FirebaseAuth mAuth;
    FirebaseUser currentUser;
    FirebaseFirestore mFirestore;
    DocumentReference mReference;
    private Uri pickImage = null;
    ImageView image_post, cancel, image, image_close;
    TextView detail_title, detail_deskripsi, detail_date;
    Button btn_save;
    EditText et_title, et_deskripsi;
    Dialog popupAdd;
    BottomSheetDialog bottomSheetDialog;
    String EkskulID = "";
    Query query;
    RecyclerView recyclerView;
    FirestoreRecyclerAdapter adapter;
    ArrayList<SubEkstrakurikuler> subEkskul = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_ekskul);
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        mFirestore = FirebaseFirestore.getInstance();
        mReference = mFirestore.collection("SUB_EKSKUL").document();
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getIntent().getStringExtra("TITLE"));
        recyclerView = findViewById(R.id.recycler_sub_ekskul);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(),
                LinearLayoutManager.VERTICAL, false));

        recyclerView.setHasFixedSize(true);
        spotDialog = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage("Please wait...")
                .setCancelable(false)
                .build();
        spotDialog.dismiss();
        initPopUp();
        setupPopupImage();
        EkskulID = getIntent().getStringExtra("EkskulID");
        if (getIntent() != null) {
            EkskulID = getIntent().getStringExtra("EkskulID");
        }
        if (!EkskulID.isEmpty() && EkskulID != null) {
            loadSubEkskul();
        }
        loadDetail();
        loadSub();
    }

    private void loadSub() {
        Query query = mFirestore.collection("SUB_EKSKUL")
//                .whereEqualTo("ekskul_id", EkskulID)
                .orderBy("timeStamp", Query.Direction.DESCENDING);
        query.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                if (queryDocumentSnapshots.isEmpty()) {
                    Log.e("Data Kosong", "");
                    return;
                } else {
                    List<SubEkstrakurikuler> ekstrakurikulers = queryDocumentSnapshots.toObjects(SubEkstrakurikuler.class);
                    subEkskul.addAll(ekstrakurikulers);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("ERROR", e.getMessage());
            }
        });
    }


    private void loadDetail() {
        bottomSheetDialog = new BottomSheetDialog(this);
        bottomSheetDialog.setContentView(R.layout.sub_detail);
        bottomSheetDialog.getWindow().setLayout(Toolbar.LayoutParams.MATCH_PARENT, Toolbar.LayoutParams.WRAP_CONTENT);
        bottomSheetDialog.getWindow().getAttributes().gravity = Gravity.TOP;
        detail_title = bottomSheetDialog.findViewById(R.id.detail_title);
        detail_deskripsi = bottomSheetDialog.findViewById(R.id.detail_deskripsi);
        detail_date = bottomSheetDialog.findViewById(R.id.detail_date);
        image_close = bottomSheetDialog.findViewById(R.id.image_close);
        image = bottomSheetDialog.findViewById(R.id.image);
        image_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.cancel();
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    private void loadSubEkskul() {
        query = FirebaseFirestore.getInstance().collection("SUB_EKSKUL")
                .whereEqualTo("ekskul_id", EkskulID)
                .orderBy("timeStamp", Query.Direction.DESCENDING);
        FirestoreRecyclerOptions<SubEkstrakurikuler> options = new FirestoreRecyclerOptions.Builder<SubEkstrakurikuler>()
                .setQuery(query, SubEkstrakurikuler.class)
                .build();
        adapter = new FirestoreRecyclerAdapter<SubEkstrakurikuler, SubViewHolder>(options) {

            @NonNull
            @Override
            public SubViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {
                View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_sub_ekskul, group, false);
                return new SubViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull SubViewHolder holder, int position, @NonNull SubEkstrakurikuler model) {
                holder.nameView.setText(model.getTitle());
                Glide.with(getBaseContext()).load(model.getImage_sub_ekskul()).into(holder.imageView);
                holder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View v, int adapterPosition, boolean b) {
                        detail_title.setText(model.getTitle());
                        detail_deskripsi.setText(model.getDeskripsi());
                        detail_date.setText(model.getTimeStamp().toString());
                        Glide.with(getBaseContext()).load(model.getImage_sub_ekskul()).into(image);
                        showMessage("" + model.getTitle());
                        showDetail();
                    }
                });
            }
        };
        recyclerView.setAdapter(adapter);

    }

    private void showDetail() {
        bottomSheetDialog.show();
    }

    private void setupPopupImage() {
        image_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAndRequestPermission();
            }
        });
    }

    private void checkAndRequestPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Toast.makeText(this, "Please accept for required permission", Toast.LENGTH_SHORT).show();
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        PreqCode);
            }
        } else {
            openGallery();
        }
    }

    private void openGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, REQUESTCODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUESTCODE && data != null) {
            pickImage = data.getData();
            image_post.setImageURI(pickImage);
        }
    }

    private void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        actionMenu = menu;
        actionMenu.findItem(R.id.add).setVisible(true);
        actionMenu.findItem(R.id.report).setVisible(false);
        actionMenu.findItem(R.id.absensi).setVisible(false);
        actionMenu.findItem(R.id.changePassword).setVisible(false);
        actionMenu.findItem(R.id.logout).setVisible(false);
        actionMenu.findItem(R.id.pembina).setVisible(false);
        actionMenu.findItem(R.id.siswa).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.add) {
            showAddDialog();
        }
        return super.onOptionsItemSelected(item);
    }

    private void showAddDialog() {
        popupAdd.show();
    }

    private void initPopUp() {
        popupAdd = new Dialog(this);
        popupAdd.setContentView(R.layout.bottom_add_sub_ekskul);
        popupAdd.getWindow().setLayout(Toolbar.LayoutParams.MATCH_PARENT, Toolbar.LayoutParams.WRAP_CONTENT);
        popupAdd.getWindow().getAttributes().gravity = Gravity.TOP;

        image_post = popupAdd.findViewById(R.id.image_post);
        et_title = popupAdd.findViewById(R.id.et_title);
        et_deskripsi = popupAdd.findViewById(R.id.et_deskripsi);
        btn_save = popupAdd.findViewById(R.id.btn_save);
        cancel = popupAdd.findViewById(R.id.cancel);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupAdd.cancel();
            }
        });
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!et_title.getText().toString().isEmpty() && !et_deskripsi.getText().toString().isEmpty() && image_post != null) {
                    spotDialog.show();
                    StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("sub_ekskul_image");
                    final StorageReference imageFilePath = storageReference.child(pickImage.getLastPathSegment());
                    imageFilePath.putFile(pickImage).addOnSuccessListener(taskSnapshot -> imageFilePath.getDownloadUrl().addOnSuccessListener(uri -> {
                        spotDialog.show();
                        String imageDownloadLink = uri.toString();
                        String ekskul_id = getIntent().getStringExtra("EkskulID");
                        //create post object
                        SubEkstrakurikuler upload = new SubEkstrakurikuler(et_title.getText().toString(),
                                et_deskripsi.getText().toString(),
                                imageDownloadLink,
                                currentUser.getUid(),
                                ekskul_id
                        );
                        //add post to firebase database
                        addUpload(upload);
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            spotDialog.dismiss();
                            showMessage(e.getMessage());
                        }
                    }));
                } else {
                    spotDialog.dismiss();
                    showMessage("Please verify all input and choose Post Image!");
                }
            }
        });
    }

    private void addUpload(SubEkstrakurikuler subEkstrakurikuler) {
        FirebaseFirestore mFirestore = FirebaseFirestore.getInstance();
        DocumentReference mReference = mFirestore.collection("SUB_EKSKUL").document();
        String key = mReference.getId();
        subEkstrakurikuler.setId_sub_ekskul(key);
        mReference.set(subEkstrakurikuler).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                spotDialog.dismiss();
                showMessage("Successfully!");
                popupAdd.dismiss();
            }
        });
    }

}
