package com.example.smkdaarululuum.model;

import android.widget.RadioGroup;

import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class Absensi {
    private String id_absensi;
    private String nisn;
    private String name;
    private String kelas;
    private String pembina_id;
    private String ekskul;
    private String kehadiran;
    String siswa_id;
    private @ServerTimestamp Date timeStamp;

    public Absensi() {
    }

    public Absensi(String nisn, String name, String kelas, String userId, String ekskul, String absensi, String id_siswa) {
        this.nisn = nisn;
        this.name = name;
        this.kelas = kelas;
        this.pembina_id = userId;
        this.ekskul = ekskul;
        this.kehadiran = absensi;
        this.siswa_id = id_siswa;
    }

    public String getSiswa_id() {
        return siswa_id;
    }

    public void setSiswa_id(String siswa_id) {
        this.siswa_id = siswa_id;
    }

    public String getKehadiran() {
        return kehadiran;
    }

    public void setKehadiran(String kehadiran) {
        this.kehadiran = kehadiran;
    }

    public String getId_absensi() {
        return id_absensi;
    }

    public void setId_absensi(String id_absensi) {
        this.id_absensi = id_absensi;
    }

    public String getNisn() {
        return nisn;
    }

    public void setNisn(String nisn) {
        this.nisn = nisn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getPembina_id() {
        return pembina_id;
    }

    public void setPembina_id(String pembina_id) {
        this.pembina_id = pembina_id;
    }

    public String getEkskul() {
        return ekskul;
    }

    public void setEkskul(String ekskul) {
        this.ekskul = ekskul;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
}
