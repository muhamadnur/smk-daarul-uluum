package com.example.smkdaarululuum.model;

import com.google.firebase.database.ServerValue;

public class Banner {
    private String keyBanner;
    private String title;
    private String deskripsi;
    private String image_banner;
    private String userId;
    private Object timeStamp;

    public Banner() {
    }

    public Banner(String title, String deskripsi, String image_banner, String userId) {
        this.title = title;
        this.deskripsi = deskripsi;
        this.image_banner = image_banner;
        this.userId = userId;
        this.timeStamp = ServerValue.TIMESTAMP;
    }

    public String getKeyBanner() {
        return keyBanner;
    }

    public void setKeyBanner(String keyBanner) {
        this.keyBanner = keyBanner;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getImage_banner() {
        return image_banner;
    }

    public void setImage_banner(String image_banner) {
        this.image_banner = image_banner;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Object getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Object timeStamp) {
        this.timeStamp = timeStamp;
    }
}
