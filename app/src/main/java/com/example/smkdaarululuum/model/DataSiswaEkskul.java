package com.example.smkdaarululuum.model;

import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class DataSiswaEkskul {
    private String key;
    private String nama;
    private String nisn;
    private String kelas;
    private String title;
    private String siswa_id;
    private String ekskul_id;
    private @ServerTimestamp
    Date timeStamp;

    public DataSiswaEkskul() {
    }

    public DataSiswaEkskul(String nama, String nisn, String kelas, String title, String siswa_id, String ekskul_id) {
        this.nama = nama;
        this.nisn = nisn;
        this.kelas = kelas;
        this.title = title;
        this.siswa_id = siswa_id;
        this.ekskul_id = ekskul_id;
    }

    public String getEkskul_id() {
        return ekskul_id;
    }

    public void setEkskul_id(String ekskul_id) {
        this.ekskul_id = ekskul_id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNisn() {
        return nisn;
    }

    public void setNisn(String nisn) {
        this.nisn = nisn;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSiswa_id() {
        return siswa_id;
    }

    public void setSiswa_id(String siswa_id) {
        this.siswa_id = siswa_id;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
}
