package com.example.smkdaarululuum.model;

import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

import androidx.annotation.NonNull;

public class Ekstrakurikuler {
    private String id_ekskul;
    private String title;
    private String deskripsi;
    private String image_ekskul;
    private String userId;
    private boolean isSelected;
    private @ServerTimestamp Date timeStamp;

    public Ekstrakurikuler() {
    }

    public Ekstrakurikuler(String title, String deskripsi, String image_ekskul, String userId) {
        this.title = title;
        this.deskripsi = deskripsi;
        this.image_ekskul = image_ekskul;
        this.userId = userId;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getId_ekskul() {
        return id_ekskul;
    }

    public void setId_ekskul(String id_ekskul) {
        this.id_ekskul = id_ekskul;
    }

    public String getTitle() {
        return title;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public String getImage_ekskul() {
        return image_ekskul;
    }

    public String getUserId() {
        return userId;
    }


    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public void setImage_ekskul(String image_ekskul) {
        this.image_ekskul = image_ekskul;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    @NonNull
    @Override
    public String toString() {
        return title;
    }
}
