package com.example.smkdaarululuum.model;

import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class Nilai {
    private String id_nilai;
    private String nisn;
    private String name;
    private String kelas;
    private String pembina_id;
    private String ekskul;
    private String nilai;
    private String keterangan;
    private String siswa_id;
    private @ServerTimestamp Date timeStamp;

    public Nilai() {
    }

    public Nilai(String nisn, String name, String kelas, String userId, String ekskul, String nilai, String keterangan, String siswa) {
        this.nisn = nisn;
        this.name = name;
        this.kelas = kelas;
        this.pembina_id = userId;
        this.ekskul = ekskul;
        this.nilai = nilai;
        this.keterangan = keterangan;
        this.siswa_id = siswa;
    }

    public String getSiswa_id() {
        return siswa_id;
    }

    public void setSiswa_id(String siswa_id) {
        this.siswa_id = siswa_id;
    }

    public String getId_nilai() {
        return id_nilai;
    }

    public void setId_nilai(String id_nilai) {
        this.id_nilai = id_nilai;
    }

    public String getNisn() {
        return nisn;
    }

    public void setNisn(String nisn) {
        this.nisn = nisn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getPembina_id() {
        return pembina_id;
    }

    public void setPembina_id(String pembina_id) {
        this.pembina_id = pembina_id;
    }

    public String getEkskul() {
        return ekskul;
    }

    public void setEkskul(String ekskul) {
        this.ekskul = ekskul;
    }

    public String getNilai() {
        return nilai;
    }

    public void setNilai(String nilai) {
        this.nilai = nilai;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
}
