package com.example.smkdaarululuum.model;

import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class Pembina {
    private String id_pembina;
    private String name;
    private String email;
    private String phone;
    private String alamat;
    private String users_id;
    private String ekskul;
    private @ServerTimestamp
    Date timeStamp;

    public Pembina() {
    }

    public Pembina(String name, String email, String phone, String alamat, String users_id, String ekskul) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.alamat = alamat;
        this.users_id = users_id;
        this.ekskul = ekskul;
    }

    public String getEkskul() {
        return ekskul;
    }

    public void setEkskul(String ekskul) {
        this.ekskul = ekskul;
    }

    public String getId_pembina() {
        return id_pembina;
    }

    public void setId_pembina(String id_pembina) {
        this.id_pembina = id_pembina;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getUsers_id() {
        return users_id;
    }

    public void setUsers_id(String users_id) {
        this.users_id = users_id;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
}
