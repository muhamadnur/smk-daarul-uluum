package com.example.smkdaarululuum.model;

import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class Siswa {
    private String id_siswa;
    private String nisn;
    private String email;
    private String kelas;
    private String name;
    private String phone;
    private String alamat;
    private String users_id;
    private @ServerTimestamp Date timeStamp;

    public Siswa() {
    }

    public Siswa(String nisn,String name, String email, String kelas, String phone, String alamat, String users_id) {
        this.nisn = nisn;
        this.email = email;
        this.kelas = kelas;
        this.name = name;
        this.phone = phone;
        this.alamat = alamat;
        this.users_id = users_id;
    }

    public String getId_siswa() {
        return id_siswa;
    }

    public void setId_siswa(String id_siswa) {
        this.id_siswa = id_siswa;
    }

    public String getNisn() {
        return nisn;
    }

    public void setNisn(String nisn) {
        this.nisn = nisn;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getUsers_id() {
        return users_id;
    }

    public void setUsers_id(String users_id) {
        this.users_id = users_id;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
}
