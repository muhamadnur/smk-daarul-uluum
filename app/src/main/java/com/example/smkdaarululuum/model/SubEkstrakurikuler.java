package com.example.smkdaarululuum.model;

import com.google.firebase.database.ServerValue;
import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class SubEkstrakurikuler {
    private String title;
    private String deskripsi;
    private String image_sub_ekskul;
    private String id_user;
    private String ekskul_id;
    private String id_sub_ekskul;
    private @ServerTimestamp Date timeStamp;

    public SubEkstrakurikuler() {
    }

    public SubEkstrakurikuler(String title, String deskripsi, String imageDownloadLink, String uid, String ekskul_id) {
        this.title = title;
        this.deskripsi = deskripsi;
        this.image_sub_ekskul = imageDownloadLink;
        this.id_user = uid;
        this.ekskul_id = ekskul_id;
    }

    public String getId_sub_ekskul() {
        return id_sub_ekskul;
    }

    public void setId_sub_ekskul(String id_sub_ekskul) {
        this.id_sub_ekskul = id_sub_ekskul;
    }

    public String getEkskul_id() {
        return ekskul_id;
    }

    public void setEkskul_id(String ekskul_id) {
        this.ekskul_id = ekskul_id;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getImage_sub_ekskul() {
        return image_sub_ekskul;
    }

    public void setImage_sub_ekskul(String image_sub_ekskul) {
        this.image_sub_ekskul = image_sub_ekskul;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
}

