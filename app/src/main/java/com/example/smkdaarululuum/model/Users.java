package com.example.smkdaarululuum.model;

import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class Users {
    private String id_users;
    private String level;
    private String email;
    private @ServerTimestamp
    Date timeStamp;

    public Users() {
    }

    public Users(String id_users, String level, String email) {
        this.id_users = id_users;
        this.level = level;
        this.email = email;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }


    public String getId_users() {
        return id_users;
    }

    public void setId_users(String id_users) {
        this.id_users = id_users;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
