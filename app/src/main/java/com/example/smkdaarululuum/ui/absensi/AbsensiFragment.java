package com.example.smkdaarululuum.ui.absensi;

import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dmax.dialog.SpotsDialog;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.smkdaarululuum.Interface.ItemClickListener;
import com.example.smkdaarululuum.R;
import com.example.smkdaarululuum.model.Absensi;
import com.example.smkdaarululuum.model.DataSiswaEkskul;
import com.example.smkdaarululuum.viewholder.AbsensiSiswaViewHolder;
import com.example.smkdaarululuum.viewholder.SiswaViewHolder;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.internal.$Gson$Preconditions;

import java.util.ArrayList;
import java.util.List;

public class AbsensiFragment extends Fragment {
    private FirebaseFirestore mStore;
    TextView tv_name, tv_nisn, tv_kelas, siswa_id, tv_ekskul, absensi_ID;
    RadioGroup radioGroup;
    Button btn_ubah, btn_hapus;
    RadioButton r_hadir, r_izin, r_alpa, r_sakit, r_absensi;
    ImageView img_cancel;
    FirebaseAuth mAuth;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    FirestoreRecyclerAdapter adapter;
    DocumentReference mReference;
    Dialog popupAbsen;
    ArrayList<Absensi> absensiArrayList = new ArrayList<>();
    String userID = "";
    AlertDialog spotDialog;
    MediaPlayer absensi;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_absensi, container, false);
        absensi = MediaPlayer.create(getActivity(),R.raw.absen);
        absensi.start();
        mStore = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        mReference = mStore.collection("absensi_ekskul").document();
        userID = mAuth.getCurrentUser().getUid();
        recyclerView = root.findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        spotDialog = new SpotsDialog.Builder()
                .setContext(getActivity())
                .setMessage("Please wait...")
                .setCancelable(false)
                .build();
        spotDialog.dismiss();
        loadDataAbsensi();
        loadAbsensi();
        setHasOptionsMenu(true);
        return root;
    }

    private void showDataAbsensi() {
        Query query = mStore.collection("absensi_ekskul")
                .orderBy("timeStamp", Query.Direction.DESCENDING);
        query.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                if (queryDocumentSnapshots.isEmpty()) {
                    Log.e("Data Kosong", "");
                    return;
                } else {
                    List<Absensi> absensis = queryDocumentSnapshots.toObjects(Absensi.class);
                    absensiArrayList.addAll(absensis);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("ERROR", e.getMessage());
            }
        });
    }

    private void loadDataAbsensi() {
        popupAbsen = new Dialog(getActivity());
        popupAbsen.setContentView(R.layout.popup_absensi);
        popupAbsen.getWindow().setLayout(Toolbar.LayoutParams.MATCH_PARENT, Toolbar.LayoutParams.WRAP_CONTENT);
        popupAbsen.getWindow().getAttributes().gravity = Gravity.TOP;
        img_cancel = popupAbsen.findViewById(R.id.img_cancel);
        tv_name = popupAbsen.findViewById(R.id.tv_name);
        tv_nisn = popupAbsen.findViewById(R.id.tv_nisn);
        tv_kelas = popupAbsen.findViewById(R.id.tv_kelas);
        tv_ekskul = popupAbsen.findViewById(R.id.tv_ekskul);
        siswa_id = popupAbsen.findViewById(R.id.siswa_id);
        r_hadir = popupAbsen.findViewById(R.id.r_hadir);
        r_sakit = popupAbsen.findViewById(R.id.r_sakit);
        r_izin = popupAbsen.findViewById(R.id.r_izin);
        r_alpa = popupAbsen.findViewById(R.id.r_alpa);
        absensi_ID = popupAbsen.findViewById(R.id.absensi_id);
        radioGroup = popupAbsen.findViewById(R.id.radioGroup);
        btn_ubah = popupAbsen.findViewById(R.id.btn_ubah);
        btn_hapus = popupAbsen.findViewById(R.id.btn_hapus);
        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupAbsen.cancel();
            }
        });
        btn_hapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spotDialog.show();
                String absensi = absensi_ID.getText().toString();
                mReference = mStore.collection("absensi_ekskul").document(absensi);
                mReference.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            spotDialog.dismiss();
                            popupAbsen.dismiss();
                            showMessage("Deleted!");
                            showDataAbsensi();
                        } else {
                            spotDialog.dismiss();
                            popupAbsen.dismiss();
                            showMessage(task.getException().getMessage());
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
            }
        });
        btn_ubah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spotDialog.show();
                int selectedID = radioGroup.getCheckedRadioButtonId();
                r_absensi = popupAbsen.findViewById(selectedID);
                String radio = r_absensi.getText().toString();
                String id = absensi_ID.getText().toString();
                mReference = mStore.collection("absensi_ekskul").document(id);
                mReference.update(
                        "kehadiran", radio)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    spotDialog.dismiss();
                                    popupAbsen.dismiss();
                                    showMessage("Updated!");
                                } else {
                                    spotDialog.dismiss();
                                    popupAbsen.dismiss();
                                    showMessage(task.getException().getMessage());
                                }
                            }
                        });
            }
        });
    }

    private void loadAbsensi() {
        Query query = FirebaseFirestore.getInstance().collection("absensi_ekskul")
                .whereEqualTo("pembina_id", userID)
                .orderBy("timeStamp", Query.Direction.DESCENDING);
        FirestoreRecyclerOptions<Absensi> options = new FirestoreRecyclerOptions.Builder<Absensi>()
                .setQuery(query, Absensi.class)
                .build();
        adapter = new FirestoreRecyclerAdapter<Absensi, AbsensiSiswaViewHolder>(options) {

            @NonNull
            @Override
            public AbsensiSiswaViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {
                View view = LayoutInflater.from(group.getContext()).inflate(R.layout.view_absensi_siswa, group, false);
                return new AbsensiSiswaViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull AbsensiSiswaViewHolder holder, int position, @NonNull Absensi model) {
                holder.tv_name_siswa.setText(model.getName());
                holder.tv_absensi.setText(model.getKehadiran());
                holder.tv_name_ekskul.setText(model.getEkskul());
                holder.tv_date.setText(model.getTimeStamp().toLocaleString());
                if (model.getKehadiran().equals("Izin")) {
                    holder.tv_absensi.setBackgroundColor(Color.MAGENTA);
                }
                if (model.getKehadiran().equals("Sakit")) {
                    holder.tv_absensi.setBackgroundColor(Color.YELLOW);
                    holder.tv_absensi.setTextColor(Color.BLACK);
                }
                if (model.getKehadiran().equals("Hadir")) {
                    holder.tv_absensi.setBackgroundColor(Color.GREEN);
                }
                if (model.getKehadiran().equals("Alpa")) {
                    holder.tv_absensi.setBackgroundColor(Color.RED);
                }
                holder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View v, int adapterPosition, boolean b) {
                        showMessage(model.getName());
                        absensi_ID.setText(model.getId_absensi());
                        tv_nisn.setText(model.getNisn());
                        tv_name.setText(model.getName());
                        tv_kelas.setText(model.getKelas());
                        tv_ekskul.setText(model.getEkskul());
                        if (model.getKehadiran().equals("Hadir")) {
                            r_hadir.setChecked(true);
                        } else if (model.getKehadiran().equals("Izin")) {
                            r_izin.setChecked(true);
                        } else if (model.getKehadiran().equals("Sakit")) {
                            r_sakit.setChecked(true);
                        } else {
                            r_alpa.setChecked(true);
                        }
                        updateData();
                    }
                });
            }
        };
        recyclerView.setAdapter(adapter);
    }

    private void updateData() {
        popupAbsen.show();
    }

    private void showMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        MenuItem item = menu.findItem(R.id.add);
        if (item != null)
            item.setVisible(false);
    }
}
