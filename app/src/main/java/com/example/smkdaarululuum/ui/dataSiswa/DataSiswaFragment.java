package com.example.smkdaarululuum.ui.dataSiswa;

import android.app.AlertDialog;
import android.app.Dialog;
import android.media.MediaPlayer;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dmax.dialog.SpotsDialog;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.smkdaarululuum.Common;
import com.example.smkdaarululuum.Interface.ItemClickListener;
import com.example.smkdaarululuum.R;
import com.example.smkdaarululuum.model.Absensi;
import com.example.smkdaarululuum.model.DataSiswaEkskul;
import com.example.smkdaarululuum.model.Nilai;
import com.example.smkdaarululuum.model.Pembina;
import com.example.smkdaarululuum.viewholder.SiswaViewHolder;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.mancj.materialsearchbar.MaterialSearchBar;

import java.util.ArrayList;
import java.util.List;


public class DataSiswaFragment extends Fragment {
    FirebaseFirestore mStore;
    DocumentReference mReference;
    FirebaseAuth mAuth;
    String userID = "";
    String EKSKUL = "";
    FirestoreRecyclerAdapter<DataSiswaEkskul, SiswaViewHolder> adapter;
    FirestoreRecyclerAdapter<DataSiswaEkskul, SiswaViewHolder> searchAdapter;
    FirestoreRecyclerOptions<DataSiswaEkskul> options;
    RecyclerView recycler_absensi;
    RecyclerView.LayoutManager layoutManager;
    TextView tv_name, tv_nisn, tv_kelas, siswa_id, tv_ekskul, namaEkskul;
    Button btn_simpanNilai;
    ImageView img_cancel;
    EditText et_nilai, et_keterangan;
    RadioGroup radioGroup, r_group;
    RadioButton radioButton, r_absensi;
    Dialog popupNilai;
    LinearLayout linear, linearLayout;
    AlertDialog spotDialog;
    Query query;
    ArrayList<Pembina> pembinaArrayList = new ArrayList<>();
    ArrayList<DataSiswaEkskul> siswaEkskuls = new ArrayList<>();
    MediaPlayer dataSiswa;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_data_siswa, container, false);
        dataSiswa = MediaPlayer.create(getActivity(),R.raw.data_siswa);
        dataSiswa.start();
        mStore = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        userID = mAuth.getCurrentUser().getUid();
        mReference = mStore.collection("pembina").document(userID);
        recycler_absensi = root.findViewById(R.id.recycler_absensi);
        layoutManager = new LinearLayoutManager(getActivity());
        recycler_absensi.setLayoutManager(layoutManager);
//        namaEkskul = root.findViewById(R.id.namaEkskul);
//        namaEkskul.setText(userID);
        spotDialog = new SpotsDialog.Builder()
                .setContext(getActivity())
                .setMessage("Please wait...")
                .setCancelable(false)
                .build();
        spotDialog.dismiss();
//        materialSearchBar = root.findViewById(R.id.searchBar);
//        materialSearchBar.setHint("Search");
        loadSiswa();

        setHasOptionsMenu(true);
        loadPopupNilai();
        showDataSiswa();
        return root;
    }

    private void showDataSiswa() {

        Query query = FirebaseFirestore.getInstance().collection("daftarEkskul")
//                .whereEqualTo("title", EKSKUL)
                .orderBy("timeStamp", Query.Direction.DESCENDING);
        query.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                if (queryDocumentSnapshots.isEmpty()) {
                    Log.e("Data Kosong", "");
                    return;
                } else {
                    List<DataSiswaEkskul> ekskuls = queryDocumentSnapshots.toObjects(DataSiswaEkskul.class);
                    siswaEkskuls.addAll(ekskuls);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e("ERROR", e.getMessage());
            }
        });
    }

    private void loadSiswa() {
        query = FirebaseFirestore.getInstance().collection("daftarEkskul")
//                .whereEqualTo("title", EKSKUL)
                .orderBy("nama", Query.Direction.ASCENDING);
        options = new FirestoreRecyclerOptions.Builder<DataSiswaEkskul>()
                .setQuery(query, DataSiswaEkskul.class)
                .build();
        adapter = new FirestoreRecyclerAdapter<DataSiswaEkskul, SiswaViewHolder>(options) {

            @NonNull
            @Override
            public SiswaViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {
                View view = LayoutInflater.from(group.getContext()).inflate(R.layout.item_siswa, group, false);
                SiswaViewHolder viewHolder = new SiswaViewHolder(view);
                return viewHolder;
            }

            @Override
            protected void onBindViewHolder(@NonNull SiswaViewHolder holder, int position, @NonNull DataSiswaEkskul model) {
                holder.kelasView.setText(model.getKelas());
                holder.nameView.setText(model.getNama());
                holder.nisnView.setText(model.getNisn());
                holder.ekskulView.setText(model.getTitle());
                holder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View v, int adapterPosition, boolean b) {
                        tv_nisn.setText(model.getNisn());
                        tv_name.setText(model.getNama());
                        tv_kelas.setText(model.getKelas());
                        tv_ekskul.setText(model.getTitle());
                        siswa_id.setText(model.getSiswa_id());
                        showMessage(model.getNama());
                        addNilai();
                    }
                });
            }
        };
        recycler_absensi.setAdapter(adapter);
    }

    private void addNilai() {
        popupNilai.show();
    }

    private void loadPopupNilai() {
        popupNilai = new Dialog(getActivity());
        popupNilai.setContentView(R.layout.popup_nilai);
        popupNilai.getWindow().setLayout(Toolbar.LayoutParams.MATCH_PARENT, Toolbar.LayoutParams.WRAP_CONTENT);
        popupNilai.getWindow().getAttributes().gravity = Gravity.TOP;
        img_cancel = popupNilai.findViewById(R.id.img_cancel);
        radioGroup = popupNilai.findViewById(R.id.radioGroup);
        r_group = popupNilai.findViewById(R.id.r_group);
        tv_name = popupNilai.findViewById(R.id.tv_name);
        tv_nisn = popupNilai.findViewById(R.id.tv_nisn);
        tv_kelas = popupNilai.findViewById(R.id.tv_kelas);
        et_nilai = popupNilai.findViewById(R.id.et_nilai);
        tv_ekskul = popupNilai.findViewById(R.id.tv_ekskul);
        et_keterangan = popupNilai.findViewById(R.id.et_keterangan);
        siswa_id = popupNilai.findViewById(R.id.siswa_id);
        linear = popupNilai.findViewById(R.id.linear);
        linearLayout = popupNilai.findViewById(R.id.linear1);
        linear.setVisibility(View.GONE);
//        linearLayout.setVisibility(View.GONE);
        btn_simpanNilai = popupNilai.findViewById(R.id.btn_simpanNilai);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int selectedID = radioGroup.getCheckedRadioButtonId();
                radioButton = popupNilai.findViewById(selectedID);
                String radio = radioButton.getText().toString();
                if (radio.equals("Absensi")) {
                    linear.setVisibility(View.GONE);
                    linearLayout.setVisibility(View.VISIBLE);
                }
                if (radio.equals("Nilai")) {
                    linear.setVisibility(View.VISIBLE);
                    linearLayout.setVisibility(View.GONE);
                }
            }
        });
        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupNilai.cancel();
            }
        });
        btn_simpanNilai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spotDialog.show();
                int selectedID = radioGroup.getCheckedRadioButtonId();
                radioButton = popupNilai.findViewById(selectedID);
                String radio = radioButton.getText().toString();
                String nisn = tv_nisn.getText().toString().trim();
                String name = tv_name.getText().toString().trim();
                String kelas = tv_kelas.getText().toString().trim();
                String mNilai = et_nilai.getText().toString().trim();
                String keterangan = et_keterangan.getText().toString();
                String id_siswa = siswa_id.getText().toString();
                String ekskul = tv_ekskul.getText().toString();
                if (radio.equals("Absensi")) {
                    int selectAbsensi = r_group.getCheckedRadioButtonId();
                    r_absensi = popupNilai.findViewById(selectAbsensi);
                    String absen = r_absensi.getText().toString();
                    if (!absen.isEmpty()) {
                        Absensi absensi = new Absensi(nisn, name, kelas, userID, ekskul, absen, id_siswa);
                        addAbsenSiswa(absensi);
                    } else {
                        spotDialog.dismiss();
                        showMessage("Failed!");
                    }
                }
                if (radio.equals("Nilai")) {
                    Nilai nilai = new Nilai(nisn, name, kelas, userID, ekskul, mNilai, keterangan, id_siswa);
                    addDataNilai(nilai);
                }
            }
        });
    }

    private void addAbsenSiswa(Absensi absensi) {
        FirebaseFirestore mFirestore = FirebaseFirestore.getInstance();
        DocumentReference mReference = mFirestore.collection("absensi_ekskul").document();
        String key = mReference.getId();
        absensi.setId_absensi(key);
        mReference.set(absensi).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                showMessage("Successfully");
                popupNilai.dismiss();
                spotDialog.dismiss();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                showMessage(e.getMessage());
                spotDialog.dismiss();
            }
        });
    }

    private void addDataNilai(Nilai nilai) {
        FirebaseFirestore mFirestore = FirebaseFirestore.getInstance();
        DocumentReference mReference = mFirestore.collection("nilai_ekskul").document();
        String key = mReference.getId();
        nilai.setId_nilai(key);
        mReference.set(nilai).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                showMessage("Successfully");
                popupNilai.dismiss();
                spotDialog.dismiss();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                showMessage(e.getMessage());
                spotDialog.dismiss();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }


    private void showMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        MenuItem item = menu.findItem(R.id.search);
        MenuItem item1 = menu.findItem(R.id.add);
        if (item != null) {
            item.setVisible(true);
        }
        if (item1 != null) {
            item1.setVisible(false);
        }

    }

}
