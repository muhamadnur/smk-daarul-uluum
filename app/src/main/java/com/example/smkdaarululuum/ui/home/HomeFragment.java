package com.example.smkdaarululuum.ui.home;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.example.smkdaarululuum.Interface.ItemClickListener;
import com.example.smkdaarululuum.R;

import com.example.smkdaarululuum.activity.sub_ekstrakurikuler.SubEkskulActivity;
import com.example.smkdaarululuum.model.Banner;
import com.example.smkdaarululuum.model.Ekstrakurikuler;
import com.example.smkdaarululuum.viewholder.HomeViewHolder;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.HashMap;

public class HomeFragment extends Fragment {
    private RecyclerView recyclerView;
    private SliderLayout sliderLayout;
    FirebaseAuth mAuth;
    FirebaseDatabase firebaseDatabase;
    private FirebaseFirestore mFirestore;
//    private CollectionReference mReference;
    HashMap<String,String>image_list;
    FirestoreRecyclerAdapter adapter;
    DocumentReference mReference;
    MediaPlayer paskibra,pramuka,pentaque,pencak,rohis,utama;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);
        recyclerView = root.findViewById(R.id.recyclerview_ekskul);
        mFirestore = FirebaseFirestore.getInstance();
        mReference = mFirestore.collection("EKSKUL").document();
        sliderLayout = root.findViewById(R.id.slider);
        rohis = MediaPlayer.create(getActivity(), R.raw.rohis);
        pramuka = MediaPlayer.create(getActivity(), R.raw.pramuka);
        pencak = MediaPlayer.create(getActivity(), R.raw.pencak_silat);
        pentaque = MediaPlayer.create(getActivity(), R.raw.pentaque);
        paskibra = MediaPlayer.create(getActivity(), R.raw.paskibra);
        utama = MediaPlayer.create(getActivity(),R.raw.utama);
        utama.start();
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
        setHasOptionsMenu(true);
        loadEkskul();
        setUpSlider();
        return root;
    }

    private void loadEkskul() {
        Query query = FirebaseFirestore.getInstance().collection("EKSKUL").
                orderBy("timeStamp", Query.Direction.ASCENDING);
        FirestoreRecyclerOptions<Ekstrakurikuler> options = new FirestoreRecyclerOptions.Builder<Ekstrakurikuler>().
                setQuery(query, Ekstrakurikuler.class)
                .build();
        adapter = new FirestoreRecyclerAdapter<Ekstrakurikuler,HomeViewHolder>(options) {

            @NonNull
            @Override
            public HomeViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {
                View view = LayoutInflater.from(group.getContext()).inflate(R.layout.layout_ekstrakurikuler,group,false);
                return new HomeViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull HomeViewHolder holder, int position, @NonNull Ekstrakurikuler model) {
                holder.nameView.setText(model.getTitle());
                Glide.with(getContext()).load(model.getImage_ekskul()).into(holder.imageView);
                holder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int adapterPosition, boolean b) {
                        Intent intent = new Intent(getActivity(), SubEkskulActivity.class);
                        intent.putExtra("EkskulID",model.getId_ekskul());
                        intent.putExtra("TITLE",model.getTitle());
                        String suara = model.getTitle();
                        if (suara.equals("Paskibra")){
                            paskibra.start();
                        }else if (suara.equals("Pramuka")){
                            pramuka.start();
                        }else if (suara.equals("Rohis")){
                            rohis.start();
                        }else if (suara.equals("Pencak Silat")){
                            pencak.start();
                        }else {
                            pentaque.start();
                        }
                        startActivity(intent);
                        Toast.makeText(getActivity(),model.getTitle(),Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        recyclerView.setAdapter(adapter);
    }

    private void setUpSlider() {
        firebaseDatabase = FirebaseDatabase.getInstance();
//        databaseReference = firebaseDatabase.getReference("Banner");
        image_list = new HashMap<>();
        final DatabaseReference banner = firebaseDatabase.getReference("Banner");
        banner.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postDataBanner : dataSnapshot.getChildren()){
                    Banner listBanner = postDataBanner.getValue(Banner.class);
                    image_list.put(listBanner.getTitle()+"_"+listBanner.getKeyBanner(),listBanner.getImage_banner());
                }
                for (String key:image_list.keySet()){
                    String[] keySplit = key.split("_");
                    String title = keySplit[0];
                    final String idBanner = keySplit[1];

                    final TextSliderView textSliderView = new TextSliderView(getContext());
//                    description(title)
                    textSliderView
                            .image(image_list.get(key))
                            .setScaleType(BaseSliderView.ScaleType.Fit)
                            .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                @Override
                                public void onSliderClick(BaseSliderView slider) {
//                                    Intent intent = new Intent(getActivity(), TestingActivity.class);
//                                    intent.putExtra(textSliderView.getBundle().getString());
//                                    startActivity(intent);
                                    Toast.makeText(getActivity(),"Clicked",Toast.LENGTH_SHORT).show();
                                }
                            });
                    textSliderView.bundle(new Bundle());
                    textSliderView.getBundle().putString("BannerID",idBanner);
                    sliderLayout.addSlider(textSliderView);
                    //remove event after finish
                    banner.removeEventListener(this);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
//        sliderLayout.setPagerTransformer(SliderLayout.Transformer.Background2Foreground);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        sliderLayout.setDuration(4000);
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }
    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        MenuItem item=menu.findItem(R.id.add);
        if(item!=null)
            item.setVisible(false);
    }
}
