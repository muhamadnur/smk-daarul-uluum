package com.example.smkdaarululuum.ui.nilai;

import android.app.AlertDialog;
import android.app.Dialog;
import android.media.MediaPlayer;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dmax.dialog.SpotsDialog;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.smkdaarululuum.Interface.ItemClickListener;
import com.example.smkdaarululuum.R;
import com.example.smkdaarululuum.model.DataSiswaEkskul;
import com.example.smkdaarululuum.model.Nilai;
import com.example.smkdaarululuum.model.Pembina;
import com.example.smkdaarululuum.viewholder.NilaiSiswaViewHolder;
import com.example.smkdaarululuum.viewholder.SiswaViewHolder;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;


public class NilaiFragment extends Fragment {
    RecyclerView recycler_nilai;
    RecyclerView.LayoutManager layoutManager;
    Button btn_ubah, btn_hapus;
    DocumentReference mReference;
    FirebaseAuth mAuth;
    TextView tv_name, tv_nisn, tv_kelas, siswa_id, tv_ekskul, nilai_id;
    EditText et_nilai, et_keterangan;
    ImageView img_cancel;
    FirebaseFirestore mStore;
    FirestoreRecyclerAdapter adapter;
    Dialog popupNilai;
    String userID = "";
    AlertDialog spotDialog;
    MediaPlayer nilai;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_nilai, container, false);
        nilai = MediaPlayer.create(getActivity(),R.raw.nilai);
        nilai.start();
        mStore = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        userID = mAuth.getCurrentUser().getUid();
        recycler_nilai = root.findViewById(R.id.recycler_nilai);
        layoutManager = new LinearLayoutManager(getActivity());
        recycler_nilai.setLayoutManager(layoutManager);
        spotDialog = new SpotsDialog.Builder()
                .setContext(getActivity())
                .setMessage("Please wait...")
                .setCancelable(false)
                .build();
        spotDialog.dismiss();
        loadNilaiSiswa();
        setHasOptionsMenu(true);
        loadPopup();
        return root;
    }

    private void loadPopup() {
        popupNilai = new Dialog(getActivity());
        popupNilai.setContentView(R.layout.popup_edit_nilai);
        popupNilai.getWindow().setLayout(Toolbar.LayoutParams.MATCH_PARENT, Toolbar.LayoutParams.WRAP_CONTENT);
        popupNilai.getWindow().getAttributes().gravity = Gravity.TOP;
        img_cancel = popupNilai.findViewById(R.id.img_cancel);
        tv_name = popupNilai.findViewById(R.id.tv_name);
        tv_nisn = popupNilai.findViewById(R.id.tv_nisn);
        tv_kelas = popupNilai.findViewById(R.id.tv_kelas);
        et_nilai = popupNilai.findViewById(R.id.et_nilai);
        nilai_id = popupNilai.findViewById(R.id.nilai_id);
        tv_ekskul = popupNilai.findViewById(R.id.tv_ekskul);
        btn_ubah = popupNilai.findViewById(R.id.btn_ubah);
        btn_hapus = popupNilai.findViewById(R.id.btn_hapus);
        et_keterangan = popupNilai.findViewById(R.id.et_keterangan);
        siswa_id = popupNilai.findViewById(R.id.siswa_id);
        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupNilai.cancel();
            }
        });
        btn_hapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spotDialog.show();
                String nilaID = nilai_id.getText().toString();
                mReference = mStore.collection("nilai_ekskul").document(nilaID);
                mReference.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            spotDialog.dismiss();
                            popupNilai.dismiss();
                            showMessage("Deleted!");
                        } else {
                            spotDialog.dismiss();
                            popupNilai.dismiss();
                            showMessage(task.getException().getMessage());
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
            }
        });
        btn_ubah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spotDialog.show();
                String mNilai = et_nilai.getText().toString().trim();
                String keterangan = et_keterangan.getText().toString();
                String nilaID = nilai_id.getText().toString();
                if (mNilai.isEmpty()) {
                    showMessage("Nilai tidak boleh kosong");
                } else if (mNilai.matches("")) {

                } else {
                    mReference = mStore.collection("nilai_ekskul").document(nilaID);
                    mReference.update(
                            "nilai", mNilai,
                            "keterangan", keterangan)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        spotDialog.dismiss();
                                        popupNilai.dismiss();
                                        showMessage("Updated!");
                                    } else {
                                        spotDialog.dismiss();
                                        popupNilai.dismiss();
                                        showMessage(task.getException().getMessage());
                                    }
                                }
                            });
                }

            }
        });
    }

    private void loadNilaiSiswa() {
        Query query = FirebaseFirestore.getInstance().collection("nilai_ekskul")
                .whereEqualTo("pembina_id", userID)
                .orderBy("timeStamp", Query.Direction.DESCENDING);
        FirestoreRecyclerOptions<Nilai> options = new FirestoreRecyclerOptions.Builder<Nilai>()
                .setQuery(query, Nilai.class)
                .build();
        adapter = new FirestoreRecyclerAdapter<Nilai, NilaiSiswaViewHolder>(options) {

            @NonNull
            @Override
            public NilaiSiswaViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {
                View view = LayoutInflater.from(group.getContext()).inflate(R.layout.view_nilai_siswa, group, false);
                return new NilaiSiswaViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull NilaiSiswaViewHolder holder, int position, @NonNull Nilai model) {
                holder.tv_name_siswa.setText(model.getName());
                holder.tv_name_ekskul.setText(model.getEkskul());
//                holder.tv_keterangan.setText(model.getKeterangan());
                holder.tv_nilai.setText(model.getNilai());
                holder.tv_date.setText(model.getTimeStamp().toLocaleString());
                holder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View v, int adapterPosition, boolean b) {
                        showMessage(model.getName());
                        nilai_id.setText(model.getId_nilai());
                        tv_nisn.setText(model.getNisn());
                        tv_name.setText(model.getName());
                        tv_kelas.setText(model.getKelas());
                        tv_ekskul.setText(model.getEkskul());
                        et_nilai.setText(model.getNilai());
                        et_keterangan.setText(model.getKeterangan());
                        editOrDeleteData();
                    }
                });
            }
        };
        recycler_nilai.setAdapter(adapter);
    }

    private void editOrDeleteData() {
        popupNilai.show();
    }


    private void showMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        MenuItem item = menu.findItem(R.id.add);
        if (item != null)
            item.setVisible(false);
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
