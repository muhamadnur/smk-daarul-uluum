package com.example.smkdaarululuum.ui.profile;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import de.hdodenhof.circleimageview.CircleImageView;
import dmax.dialog.SpotsDialog;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.smkdaarululuum.R;
import com.example.smkdaarululuum.activity.auth.LoginActivity;
import com.example.smkdaarululuum.model.Pembina;
import com.example.smkdaarululuum.model.Siswa;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

public class ProfilePembinaFragment extends Fragment {
    TextView tv_name,tv_email,tv_phone,tv_alamat,tv_email_pembina,tv_ekskul;
    EditText et_name,et_phone,et_alamat;
    ImageView image_post,cancel;
    CircleImageView profile_image;
    Uri pickImage;
    AlertDialog spotDialog;
    FirebaseFirestore mStore;
    FirebaseAuth mAuth;
    String userID="";
    Dialog PopApEdit;
    Button btn_edit,btn_save,btn_logout;
    static int PreqCode = 1;
    static int REQUESTCODE = 1;
    DocumentReference mReference;
    CollapsingToolbarLayout collapsingToolbarLayout;
    MediaPlayer profile;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_profile_pembina, container, false);
        profile = MediaPlayer.create(getActivity(),R.raw.profile);
        profile.start();
        mAuth = FirebaseAuth.getInstance();
        mStore = FirebaseFirestore.getInstance();
        userID = mAuth.getCurrentUser().getUid();
        mReference = mStore.collection("pembina").document(userID);
        spotDialog = new SpotsDialog.Builder()
                .setContext(getActivity())
                .setMessage("Please wait...")
                .setCancelable(false)
                .build();
        spotDialog.dismiss();
        tv_name = root.findViewById(R.id.tv_name);
        tv_email = root.findViewById(R.id.tv_email);
        tv_alamat = root.findViewById(R.id.tv_alamat);
        tv_phone = root.findViewById(R.id.tv_phone);
        profile_image = root.findViewById(R.id.profile_image);
        btn_edit = root.findViewById(R.id.btn_edit);
        btn_logout = root.findViewById(R.id.btn_logout);
        collapsingToolbarLayout = root.findViewById(R.id.collapsing);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapseAppBar);
        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logOut();
            }
        });
        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editProfile();
            }
        });
        loadProfilePembina();
        iniPopUp();
        setHasOptionsMenu(true);
        return root;
    }

    private void iniPopUp() {
        mStore = FirebaseFirestore.getInstance();
        mReference = mStore.collection("pembina").document(userID);
        mReference.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                Pembina pembina = documentSnapshot.toObject(Pembina.class);
                tv_email_pembina.setText(pembina.getEmail());
                et_phone.setText(pembina.getPhone());
                et_name.setText(pembina.getName());
                et_alamat.setText(pembina.getAlamat());
                tv_ekskul.setText(pembina.getEkskul());
            }
        });
        PopApEdit = new Dialog(getActivity());
        PopApEdit.setContentView(R.layout.edit_profile_pembina);
        PopApEdit.getWindow().setLayout(Toolbar.LayoutParams.MATCH_PARENT,Toolbar.LayoutParams.WRAP_CONTENT);
        PopApEdit.getWindow().getAttributes().gravity = Gravity.TOP;
        et_name = PopApEdit.findViewById(R.id.et_name);
        et_phone = PopApEdit.findViewById(R.id.et_phone);
        et_alamat = PopApEdit.findViewById(R.id.et_alamat);
        tv_email_pembina = PopApEdit.findViewById(R.id.tv_Email);
        tv_ekskul = PopApEdit.findViewById(R.id.tv_ekskul);
        cancel = PopApEdit.findViewById(R.id.cancel);
        btn_save = PopApEdit.findViewById(R.id.btn_save);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopApEdit.cancel();
            }
        });
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = et_name.getText().toString().trim();
                String email = tv_email_pembina.getText().toString().trim();
                String phone = et_phone.getText().toString().trim();
                String alamat = et_alamat.getText().toString().trim();
                String ekskul = tv_ekskul.getText().toString().trim();
                if (!name.isEmpty() && !email.isEmpty() && !phone.isEmpty() && !alamat.isEmpty() && !ekskul.isEmpty()){
                    spotDialog.show();
                    Pembina pembina = new Pembina(name,email,phone,alamat,mAuth.getCurrentUser().getUid(),ekskul);
                    updatePembina(pembina);
                }else {
                    spotDialog.dismiss();
                    showMessage("Field is required!");
                }
            }
        });
    }

    private void updatePembina(Pembina pembina) {
        mReference = mStore.collection("pembina").document(userID);
        mReference.update(
                "name",pembina.getName(),
                "phone",pembina.getPhone(),
                "alamat",pembina.getAlamat()).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    spotDialog.dismiss();
                    PopApEdit.dismiss();
                    showMessage("Updated!");
                }else {
                    spotDialog.dismiss();
                    PopApEdit.dismiss();
                    showMessage(task.getException().getMessage());
                }
            }
        });
    }

    private void editProfile() {
        PopApEdit.show();
    }


    private void logOut() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Exit Application");
        builder.setMessage("Do you want to exit this application?");
        builder.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                spotDialog.show();
                mAuth.signOut();
                startActivity(new Intent(getActivity(), LoginActivity.class));
                showMessage("Log Out Success!");
            }
        });
        builder.setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                spotDialog.dismiss();
            }
        });
        builder.show();
    }
    private void showMessage(String message) {
        Toast.makeText(getActivity(),message,Toast.LENGTH_SHORT).show();
    }

    private void loadProfilePembina() {
        mStore = FirebaseFirestore.getInstance();
        mReference = mStore.collection("pembina").document(userID);
        mReference.addSnapshotListener(getActivity(), new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                Pembina pembina = documentSnapshot.toObject(Pembina.class);
                tv_name.setText(pembina.getName());
                tv_email.setText(pembina.getEmail());
                tv_alamat.setText(pembina.getAlamat());
                tv_phone.setText(pembina.getPhone());
            }
        });
    }
    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        MenuItem item=menu.findItem(R.id.add);
        if(item!=null)
            item.setVisible(false);
    }
}
