package com.example.smkdaarululuum.ui.siswa;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.smkdaarululuum.Interface.ItemClickListener;
import com.example.smkdaarululuum.R;
import com.example.smkdaarululuum.model.Absensi;
import com.example.smkdaarululuum.viewholder.AbsensiSiswaViewHolder;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class AbsensiSiswaFragment extends Fragment {
    RecyclerView recycler_absensi;
    String userID = "";
    FirestoreRecyclerAdapter adapter;
    FirebaseAuth mAuth;
    FirebaseFirestore mStore;
    RecyclerView.LayoutManager layoutManager;
    Query query;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_absensi_siswa, container, false);
        mAuth = FirebaseAuth.getInstance();
        mStore = FirebaseFirestore.getInstance();
        userID = mAuth.getCurrentUser().getUid();
        recycler_absensi = root.findViewById(R.id.recycler_absensi);
        layoutManager = new LinearLayoutManager(getActivity());
        recycler_absensi.setLayoutManager(layoutManager);
        loadAbsensi();
        return root;
    }

    private void loadAbsensi() {
        Query query = FirebaseFirestore.getInstance().collection("absensi_ekskul")
                .whereEqualTo("siswa_id", userID)
                .orderBy("timeStamp", Query.Direction.DESCENDING);
        FirestoreRecyclerOptions<Absensi> options = new FirestoreRecyclerOptions.Builder<Absensi>()
                .setQuery(query,Absensi.class)
                .build();
        adapter = new FirestoreRecyclerAdapter<Absensi, AbsensiSiswaViewHolder>(options){

            @NonNull
            @Override
            public AbsensiSiswaViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {
                View view = LayoutInflater.from(group.getContext()).inflate(R.layout.view_absensi_siswa,group,false);
                return new AbsensiSiswaViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull AbsensiSiswaViewHolder holder, int position, @NonNull Absensi model) {
                holder.tv_name_siswa.setText(model.getName());
                holder.tv_absensi.setText(model.getKehadiran());
                holder.tv_name_ekskul.setText(model.getEkskul());
                holder.tv_date.setText(model.getTimeStamp().toLocaleString());
                if (model.getKehadiran().equals("Izin")){
                    holder.tv_absensi.setBackgroundColor(Color.MAGENTA);
                }
                if (model.getKehadiran().equals("Sakit")){
                    holder.tv_absensi.setBackgroundColor(Color.YELLOW);
                    holder.tv_absensi.setTextColor(Color.BLACK);
                }
                if (model.getKehadiran().equals("Hadir")){
                    holder.tv_absensi.setBackgroundColor(Color.GREEN);
                }
                if (model.getKehadiran().equals("Alpa")){
                    holder.tv_absensi.setBackgroundColor(Color.RED);
                }
                holder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View v, int adapterPosition, boolean b) {
                        showMessage(model.getName());
                    }
                });
            }
        };
        recycler_absensi.setAdapter(adapter);
    }
    private void showMessage(String message) {
        Toast.makeText(getActivity(),message,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
