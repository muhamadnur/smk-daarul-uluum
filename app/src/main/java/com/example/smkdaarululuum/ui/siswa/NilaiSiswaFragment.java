package com.example.smkdaarululuum.ui.siswa;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.smkdaarululuum.Interface.ItemClickListener;
import com.example.smkdaarululuum.R;
import com.example.smkdaarululuum.model.Nilai;
import com.example.smkdaarululuum.model.Siswa;
import com.example.smkdaarululuum.model.SubEkstrakurikuler;
import com.example.smkdaarululuum.viewholder.NilaiSiswaViewHolder;
import com.example.smkdaarululuum.viewholder.SubViewHolder;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class NilaiSiswaFragment extends Fragment {
    RecyclerView recycler_nilai;
    String userID = "";
    FirestoreRecyclerAdapter adapter;
    FirebaseAuth mAuth;
    FirebaseFirestore mStore;
    RecyclerView.LayoutManager layoutManager;
    Query query;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_nilai_siswa, container, false);
        mAuth = FirebaseAuth.getInstance();
        mStore = FirebaseFirestore.getInstance();
        userID = mAuth.getCurrentUser().getUid();
        recycler_nilai = root.findViewById(R.id.recycler_nilai);
        layoutManager = new LinearLayoutManager(getActivity());
        recycler_nilai.setLayoutManager(layoutManager);
        loadNilai();
        return root;
    }

    private void loadNilai() {
        query = FirebaseFirestore.getInstance().collection("nilai_ekskul")
                .whereEqualTo("siswa_id", userID)
                .orderBy("timeStamp", Query.Direction.DESCENDING);
        FirestoreRecyclerOptions<Nilai> options = new FirestoreRecyclerOptions.Builder<Nilai>()
                .setQuery(query,Nilai.class)
                .build();
        adapter = new FirestoreRecyclerAdapter<Nilai,NilaiSiswaViewHolder>(options){

            @NonNull
            @Override
            public NilaiSiswaViewHolder onCreateViewHolder(@NonNull ViewGroup group, int viewType) {
                View view = LayoutInflater.from(group.getContext()).inflate(R.layout.view_nilai_siswa,group,false);
                return new NilaiSiswaViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull NilaiSiswaViewHolder holder, int position, @NonNull Nilai model) {
                holder.tv_name_siswa.setText(model.getName());
                holder.tv_name_ekskul.setText(model.getEkskul());
//                holder.tv_keterangan.setText(model.getKeterangan());
                holder.tv_nilai.setText(model.getNilai());
                holder.tv_date.setText(model.getTimeStamp().toLocaleString());
                holder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View v, int adapterPosition, boolean b) {
                        showMessage(model.getName());
                    }
                });
            }
        };
        recycler_nilai.setAdapter(adapter);
    }
    private void showMessage(String message) {
        Toast.makeText(getActivity(),message,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
