package com.example.smkdaarululuum.ui.siswa;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import dmax.dialog.SpotsDialog;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.smkdaarululuum.R;
import com.example.smkdaarululuum.activity.auth.LoginActivity;
import com.example.smkdaarululuum.model.DataSiswaEkskul;
import com.example.smkdaarululuum.model.Siswa;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;


public class ProfileSiswaFragment extends Fragment {
    TextView tv_name,tv_email,tv_phone,tv_alamat,tv_Email,tv_kelas;
    EditText et_name,et_phone,et_alamat,et_nisn, et_kelas;
    ImageView image_post,cancel;
    FirebaseFirestore mStore;
    FirebaseAuth mAuth;
    DocumentReference mReference;
    String userID= "";
    Button btn_edit,btn_save,btn_logout;
    CollapsingToolbarLayout collapsingToolbarLayout;
    Dialog PopApEdit;
    AlertDialog spotDialog;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_profile_siswa, container, false);
        mAuth = FirebaseAuth.getInstance();
        userID = mAuth.getCurrentUser().getUid();
        mStore = FirebaseFirestore.getInstance();
        spotDialog = new SpotsDialog.Builder()
                .setContext(getActivity())
                .setMessage("Please wait...")
                .setCancelable(false)
                .build();
        spotDialog.dismiss();
        mReference = mStore.collection("siswa").document(userID);
        tv_name = root.findViewById(R.id.tv_name);
        tv_email = root.findViewById(R.id.tv_email);
        tv_alamat = root.findViewById(R.id.tv_alamat);
        tv_phone = root.findViewById(R.id.tv_phone);
        tv_kelas = root.findViewById(R.id.tv_kelas);
        btn_edit = root.findViewById(R.id.btn_edit);
        btn_logout = root.findViewById(R.id.btn_logout);
        userID = mAuth.getCurrentUser().getUid();
        collapsingToolbarLayout = root.findViewById(R.id.collapsing);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapseAppBar);
        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logOutUser();
            }
        });
        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editProfile();
            }
        });
        loadProfile();
        iniPopUp();
        return root;
    }

    private void iniPopUp() {
        mStore = FirebaseFirestore.getInstance();
        mReference = mStore.collection("siswa").document(userID);
        mReference.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                Siswa siswa = documentSnapshot.toObject(Siswa.class);
                et_name.setText(siswa.getName());
                tv_Email.setText(siswa.getEmail());
                et_nisn.setText(siswa.getNisn());
                et_kelas.setText(siswa.getKelas());
                et_phone.setText(siswa.getPhone());
                et_alamat.setText(siswa.getAlamat());
            }
        });
        PopApEdit = new Dialog(getActivity());
        PopApEdit.setContentView(R.layout.edit_profile_siswa);
        PopApEdit.getWindow().setLayout(Toolbar.LayoutParams.MATCH_PARENT,Toolbar.LayoutParams.WRAP_CONTENT);
        PopApEdit.getWindow().getAttributes().gravity = Gravity.TOP;
//        image_post = PopApEdit.findViewById(R.id.image_post);
        et_name = PopApEdit.findViewById(R.id.et_name);
        tv_Email = PopApEdit.findViewById(R.id.tv_Email);
        et_phone = PopApEdit.findViewById(R.id.et_phone);
        et_alamat = PopApEdit.findViewById(R.id.et_alamat);
        et_nisn = PopApEdit.findViewById(R.id.et_nisn);
        et_kelas = PopApEdit.findViewById(R.id.et_kelas);
        cancel = PopApEdit.findViewById(R.id.cancel);
        btn_save = PopApEdit.findViewById(R.id.btn_save);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopApEdit.cancel();
            }
        });
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               String name = et_name.getText().toString().trim();
               String email = tv_Email.getText().toString().trim();
               String phone = et_phone.getText().toString().trim();
               String nisn = et_nisn.getText().toString().trim();
               String alamat = et_alamat.getText().toString().trim();
               String kelas = et_kelas.getText().toString().trim();
               if (!name.isEmpty() && !phone.isEmpty() && !nisn.isEmpty() && !alamat.isEmpty() && !kelas.isEmpty()){
                   spotDialog.show();
                   Siswa siswa = new Siswa(nisn,name,email,kelas,phone,alamat,mAuth.getCurrentUser().getUid());
                   updateDataSiswa(siswa);
               }else {
                   spotDialog.dismiss();
                   showMessage("Field is required!");
               }
            }
        });
    }


    private void updateDataSiswa(Siswa siswa) {
        mReference = mStore.collection("siswa").document(mAuth.getCurrentUser().getUid());
        String key = mReference.getId();
        mReference.update(
                "name",siswa.getName(),
                "nisn",siswa.getNisn(),
                "phone",siswa.getPhone(),
                "alamat",siswa.getAlamat(),
                "kelas",siswa.getKelas()).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    spotDialog.show();
                    PopApEdit.dismiss();
                    showMessage("Updated!");
                    loadProfile();
                }else {
                    spotDialog.dismiss();
                    PopApEdit.dismiss();
                    showMessage(task.getException().getMessage());
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                showMessage(e.getMessage());
            }
        });
    }
    private void editProfile() {
        PopApEdit.show();
    }

    private void logOutUser() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Exit Application");
        builder.setMessage("Do you want to exit this application?");
        builder.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                spotDialog.show();
                startActivity(new Intent(getActivity(), LoginActivity.class));
                showMessage("Log Out Success!");
            }
        });
        builder.setPositiveButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                spotDialog.dismiss();
            }
        });
        builder.show();
    }

    private void loadProfile() {
        mStore = FirebaseFirestore.getInstance();
        mReference = mStore.collection("siswa").document(userID);
        mReference.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                Siswa siswa = documentSnapshot.toObject(Siswa.class);
                tv_name.setText(siswa.getName());
                tv_email.setText(mAuth.getCurrentUser().getEmail());
                tv_phone.setText(siswa.getPhone());
                tv_alamat.setText(siswa.getAlamat());
                tv_kelas.setText(siswa.getKelas());
            }
        });

    }
    private void showMessage(String message) {
        Toast.makeText(getActivity(),message,Toast.LENGTH_SHORT).show();
    }

}
