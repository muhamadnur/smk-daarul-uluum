package com.example.smkdaarululuum.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.smkdaarululuum.Interface.ItemClickListener;
import com.example.smkdaarululuum.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AbsensiSiswaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
   public TextView tv_date,tv_name_siswa,tv_name_ekskul,tv_absensi;
    private ItemClickListener itemClickListener;
    public AbsensiSiswaViewHolder(@NonNull View itemView) {
        super(itemView);
        tv_absensi = itemView.findViewById(R.id.tv_absensi);
        tv_date = itemView.findViewById(R.id.tv_date);
        tv_name_ekskul = itemView.findViewById(R.id.tv_name_ekskul);
        tv_name_siswa = itemView.findViewById(R.id.tv_name_siswa);
        itemView.setOnClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onClick(v,getAdapterPosition(),false);
    }
}
