package com.example.smkdaarululuum.viewholder;

import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.smkdaarululuum.Interface.ItemClickListener;
import com.example.smkdaarululuum.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class DaftarEksulViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public CircleImageView circleImageView;
    public TextView textView;
    private ItemClickListener itemClickListener;

    public DaftarEksulViewHolder(@NonNull View itemView) {
        super(itemView);
        circleImageView = itemView.findViewById(R.id.image);
        textView = itemView.findViewById(R.id.tv_title);
        itemView.setOnClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onClick(v,getAdapterPosition(),false);
    }
}
