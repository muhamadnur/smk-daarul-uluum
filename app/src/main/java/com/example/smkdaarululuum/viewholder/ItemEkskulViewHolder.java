package com.example.smkdaarululuum.viewholder;

import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.smkdaarululuum.Interface.ItemClickListener;
import com.example.smkdaarululuum.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ItemEkskulViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public CheckBox checkBox;
    private ItemClickListener itemClickListener;

    public ItemEkskulViewHolder(@NonNull View itemView) {
        super(itemView);
        checkBox = itemView.findViewById(R.id.checkbox);
        itemView.setOnClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onClick(v,getAdapterPosition(),false);
    }
}
