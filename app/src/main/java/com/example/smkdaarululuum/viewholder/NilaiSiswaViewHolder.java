package com.example.smkdaarululuum.viewholder;

import android.view.View;
import android.widget.TextView;

import com.example.smkdaarululuum.Interface.ItemClickListener;
import com.example.smkdaarululuum.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class NilaiSiswaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView tv_date, tv_name_ekskul, tv_nilai, tv_keterangan, tv_name_siswa;
    private ItemClickListener itemClickListener;
    public NilaiSiswaViewHolder(@NonNull View itemView) {
        super(itemView);
        tv_name_siswa = itemView.findViewById(R.id.tv_name_siswa);
        tv_date = itemView.findViewById(R.id.tv_date);
        tv_name_ekskul = itemView.findViewById(R.id.tv_name_ekskul);
        tv_nilai = itemView.findViewById(R.id.tv_nilai);
//        tv_keterangan = itemView.findViewById(R.id.tv_keterangan);
        itemView.setOnClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onClick(v,getAdapterPosition(),false);
    }
}
