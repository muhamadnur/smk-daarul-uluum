package com.example.smkdaarululuum.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.smkdaarululuum.Interface.ItemClickListener;
import com.example.smkdaarululuum.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SiswaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView nameView,kelasView,ekskulView,nisnView;
    private ItemClickListener itemClickListener;

    public SiswaViewHolder(@NonNull View itemView) {
        super(itemView);
        nameView = itemView.findViewById(R.id.tv_name);
        kelasView = itemView.findViewById(R.id.tv_kelas);
        nisnView = itemView.findViewById(R.id.tv_nisn);
        ekskulView = itemView.findViewById(R.id.tv_ekskul);
        itemView.setOnClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onClick(v,getAdapterPosition(),false);
    }
}
